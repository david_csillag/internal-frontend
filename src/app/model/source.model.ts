import {PublisherModel} from './publisher.model';
import {ProjectModel} from './project.model';
import {RatingModel} from './rating.model';
import {TagModel} from './tag.model';

export class SourceModel{
	name: string;
	state: string;
	page_from: number;
	page_to: number;
	base_problem: string;
	motivation: string;
	method: string;
	result: string;
	summary: string;
	tags: TagModel[];
	publisher: PublisherModel;
	project: ProjectModel;
	// ratings: RatingModel;

	constructor(name: string, state: string, page_from: number, page_to: number, base_problem: string, motivation: string, method: string, result: string, summary: string, tags: TagModel[], publisher: PublisherModel, project: ProjectModel/*, ratings: RatingModel*/) {
		this.name = name;
		this.state = state;
		this.page_from = page_from;
		this.page_to = page_to;
		this.base_problem = base_problem;
		this.motivation = motivation;
		this.method = method;
		this.result = result;
		this.summary = summary;
		this.tags = tags;
		this.publisher = publisher;
		this.project = project;
		// this.ratings = ratings;
	}
}
