
export class SessionModel {
	Id: number;
	SessionStart: Date;
	SessionStop: Date;
	Token: string;
	Discriminator: string;

	constructor(Id: number, SessionStart: Date, SessionStop: Date, Token: string, Discriminator: string) {
		this.Id = Id;
		this.SessionStart = SessionStart;
		this.SessionStop = SessionStop;
		this.Token = Token;
		this.Discriminator = Discriminator;
	}
}
