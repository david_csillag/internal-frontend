
export class UserModel{
	Id : string;
	DisplayName : string;
	Email : string;

	constructor(Id: string, DisplayName: string, Email: string) {
		this.Id = Id;
		this.DisplayName = DisplayName;
		this.Email = Email;
	}
}
