import {SourceModel} from './source.model';

export class RatingModel{
	source_id: SourceModel;
	rate: number;
	comment: string;

	constructor(source_id: SourceModel, rate: number, comment: string) {
		this.source_id = source_id;
		this.rate = rate;
		this.comment = comment;
	}
}
