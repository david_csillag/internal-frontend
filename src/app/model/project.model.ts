import {CityModel} from './city.model';

export class ProjectModel{
	name : string;
	description : string;
	city_id : CityModel;

	constructor(name: string, description: string, city_id: CityModel) {
		this.name = name;
		this.description = description;
		this.city_id = city_id;
	}
}
