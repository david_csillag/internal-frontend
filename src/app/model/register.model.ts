export class RegisterModel {
	RegisterType: number;
	Email:string;
	Username: string;
	Password: string;
	Repassword: string;


	constructor( RegisterType: number, Username: string, Password: string, Repassword: string, Email: string) {
		this.RegisterType = RegisterType;
		this.Username = Username;
		this.Email=Email;
		this.Password = Password;
		this.Repassword = Repassword;
	}
}
