import {CountryModel} from './country.model';

export class ExternalAuthOptionModel{
	connected: boolean;
	email: string;
	icon: string;
	type: number;
	authId: number;


	constructor(connected: boolean, email: string, icon: string, type: number, authId: number) {
		this.connected = connected;
		this.email = email;
		this.icon = icon;
		this.type = type;
		this.authId = authId;
	}
}
