import {UserModel} from './user.model';

export class TokenOwnModel {
	id: number;
	sessionStart: Date;
	sessionStop: Date;
	token: string;
	user: UserModel;


	constructor(id: number, sessionStart: Date, sessionStop: Date, token: string, user: UserModel) {
		this.id = id;
		this.sessionStart = sessionStart;
		this.sessionStop = sessionStop;
		this.token = token;
		this.user = user;
	}
}
