import {CountryModel} from './country.model';

export class CityModel{
	name : string;
	country_id : CountryModel;

	constructor(name: string, country_id: CountryModel) {
		this.name = name;
		this.country_id = country_id;
	}
}
