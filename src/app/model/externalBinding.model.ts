import {CountryModel} from './country.model';

export class ExternalBindingModel{
	Type : string;
	Email : string;
	AuthId : number;

	constructor(Type: string, Email: string, AuthId: number) {
		this.Type = Type;
		this.Email = Email;
		this.AuthId = AuthId;
	}
}
