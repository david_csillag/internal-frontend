import {PublisherModel} from '../model/publisher.model';
import {Subject} from 'rxjs';
import {Injectable} from '@angular/core';

// @ts-ignore
@Injectable()
export class PublisherService{
	publisherChanged = new Subject<PublisherModel[]>();
	private publishers : PublisherModel[] = [];

	constructor(){}

	getPublishers(): PublisherModel[]{
		return this.publishers.slice();
	}

	setPublishers(publishers: PublisherModel[]) {
		this.publishers = publishers;
		this.publisherChanged.next(this.publishers.slice());
	}

	addRecipe(publisher: PublisherModel) {
		this.publishers.push(publisher);
		this.publisherChanged.next(this.publishers.slice());
	}

	updatePublisher(index: number, newPublisher: PublisherModel){
		this.publishers[index] = newPublisher;
		this.publisherChanged.next(this.publishers.slice());
	}

	deletePublisher(index: number) {
		this.publishers.splice(index, 1);
		this.publisherChanged.next(this.publishers.slice());
	}
}
