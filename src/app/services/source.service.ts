// @ts-ignore
import {SourceModel} from '../model/source.model';
import {Subject} from 'rxjs';
import {Injectable} from '@angular/core';

@Injectable()
export class SourceService {
	sourceChanged = new Subject<SourceModel[]>();
	private sources: SourceModel[] = [];

	constructor() {
	}

	getSources(): SourceModel[] {
		return this.sources.slice();
	}

	setSources(sources: SourceModel[]) {
		this.sources = sources;
		this.sourceChanged.next(this.sources.slice());
	}

	addRecipe(source: SourceModel) {
		this.sources.push(source);
		this.sourceChanged.next(this.sources.slice());
	}

	updateSource(index: number, newSource: SourceModel) {
		this.sources[index] = newSource;
		this.sourceChanged.next(this.sources.slice());
	}

	deleteSource(index: number) {
		this.sources.splice(index, 1);
		this.sourceChanged.next(this.sources.slice());
	}

}
