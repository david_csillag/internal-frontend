// @ts-ignore
import {ProjectModel} from '../model/project.model';
import {Subject, Subscription} from 'rxjs';
import {Injectable} from '@angular/core';
import {ProjectStorageService} from '../shared/project-storage.service';

@Injectable()
export class ProjectService{
	projectChanged = new Subject<ProjectModel[]>();
	private projects : ProjectModel[] = [];

	constructor(){}

	getProjects(): ProjectModel[]{
		return this.projects.slice();
	}

	setProjects(projects: ProjectModel[]) {
		this.projects = projects;
		this.projectChanged.next(this.projects.slice());
	}

	addRecipe(project: ProjectModel) {
		this.projects.push(project);
		this.projectChanged.next(this.projects.slice());
	}

	updateProject(index: number, newProject: ProjectModel){
		this.projects[index] = newProject;
		this.projectChanged.next(this.projects.slice());
	}

	deleteProject(index: number) {
		this.projects.splice(index, 1);
		this.projectChanged.next(this.projects.slice());
	}

}
