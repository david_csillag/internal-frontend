// Angular
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
// Components
import {BaseComponent} from './base/base.component';
// Auth

const routes: Routes = [
	{
		path: '',
		component: BaseComponent,
		// canActivate: [AuthGuard],
		children: [
			{
				path: 'userList',
				loadChildren: () => import('app/views/pages/project/project.module').then(m => m.ProjectModule)
			},
			// {
			// 	path: 'locations',
			// 	loadChildren: () => import('app/views/pages/location/location.module').then(m => m.LocationModule)
			// },
			// {
			// 	path: 'sources',
			// 	loadChildren: () => import('app/views/pages/source/source.module').then(m => m.SourceModule)
			// },
			// {
			// 	path: 'authentication',
			// 	// canActivate :[AlwaysAuthGuard],
			// 	loadChildren: () => import('app/views/pages/authentication/authentication.module').then(m => m.AuthenticationModule)
			// },
			{
				path: 'userDetails',
				// canActivate :[AlwaysAuthGuard],
				loadChildren: () => import('app/views/pages/options/options.module').then(m => m.OptionsModule)
			},
			{
				path: 'register',
				loadChildren: () => import('app/views/pages/source/source.module').then(m => m.SourceModule)
			},
			{
				path: 'login',
				loadChildren: () => import('app/views/pages/authentication/authentication.module').then(m => m.AuthenticationModule)
			},
			{
				path: 'finalize',
				loadChildren: () => import('app/views/pages/location/location.module').then(m => m.LocationModule)
			},
			// {path: 'error/:type', component: ErrorPageComponent},
			{path: '', redirectTo: 'userList', pathMatch: 'full'},
			{path: '**', redirectTo: 'userList'}
		]
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class PagesRoutingModule {
}
