import {Injectable} from '@angular/core';


import {Subject} from 'rxjs';
import {ProjectModel} from '../models/project.model';

@Injectable()
export class ProjectService {
	projectsChanged = new Subject<ProjectModel[]>();
	private projects: ProjectModel[];

	constructor() {

	}

	getProjects() {
		return this.projects.slice();
	}

	getProject(id: number) {
		return this.projects[id];
	}

	removeProject(id: number) {
		this.projects.splice(id, 1);
		this.projectsChanged.next(this.projects.slice());
	}


	addProject(recipe: ProjectModel) {
		this.projects.push(recipe);
		this.projectsChanged.next(this.projects.slice());
	}

	addProjects(projects: ProjectModel[]) {
		this.projects = projects;
		// this.projects.push(...projects);
		this.projectsChanged.next(this.projects.slice());
	}

	updateProject(id: number, newProject: ProjectModel) {
		this.projects[id] = newProject;
		this.projectsChanged.next(this.projects.slice());
	}
}
