import {Injectable} from '@angular/core';


import {Subject} from 'rxjs';
import {CityModel} from '../models/city.model';

@Injectable()
export class CityService {
	citiesChanged = new Subject<CityModel[]>();
	private cities: CityModel[] = [{
		name: 'City1',
		country_id: {
			name: 'Country1',
			code: 'Code1',
		}
	},
		{
			name:'City2',
			country_id: {
				name: 'Country1',
				code: 'Code1',
			}
		}
	];

	constructor() {

	}

	getCities() {
		return this.cities.slice();
	}

	getCity(id: number) {
		return this.cities[id];
	}

	removeCity(id: number) {
		this.cities.splice(id, 1);
		this.citiesChanged.next(this.cities.slice());
	}


	addCity(recipe: CityModel) {
		this.cities.push(recipe);
		this.citiesChanged.next(this.cities.slice());
	}

	addCitys(cities: CityModel[]) {
		this.cities = cities;
		// this.cities.push(...cities);
		this.citiesChanged.next(this.cities.slice());
	}

	updateCity(id: number, newCity: CityModel) {
		this.cities[id] = newCity;
		this.citiesChanged.next(this.cities.slice());
	}
}
