import {Injectable} from '@angular/core';


import {Subject} from 'rxjs';
import {CountryModel} from '../models/country.model';

@Injectable()
export class CountryService {
	countriesChanged = new Subject<CountryModel[]>();
	private countries: CountryModel[] = [];

	constructor() {

	}

	getCountries() {
		return this.countries.slice();
	}

	getCountry(id: number) {
		return this.countries[id];
	}

	removeCountry(id: number) {
		this.countries.splice(id, 1);
		this.countriesChanged.next(this.countries.slice());
	}


	addCountry(recipe: CountryModel) {
		this.countries.push(recipe);
		this.countriesChanged.next(this.countries.slice());
	}

	addCountries(countries: CountryModel[]) {
		this.countries = countries;
		// this.countries.push(...countries);
		this.countriesChanged.next(this.countries.slice());
	}

	updateCountry(id: number, newCountry: CountryModel) {
		this.countries[id] = newCountry;
		this.countriesChanged.next(this.countries.slice());
	}
}
