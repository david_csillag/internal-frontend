import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError} from 'rxjs/operators';
import {Observable, throwError} from 'rxjs';
import {BaseurlModel} from '../models/baseurl.model';
import {UsersModel} from '../models/users.model';
import {RegisterModel} from '../../../model/register.model';


@Injectable()
export class ServersService {

	// Base url
	bU: BaseurlModel = new BaseurlModel(false);
	baseurl = this.bU.baseurl;

	// Http Headers
	httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json; charset=utf-8',
			'Access-Control-Allow-Origin': '*'
		})
	};

	constructor(private httpClient: HttpClient) {
	}

// PUT
// 	putSource(source: SourceModel) {
// 		return this.httpClient.put<SourceModel>(this.baseurl + '/sources', source).pipe(
// 			catchError(this.errorHandl));
// 	}


// POST
// 	createSource(source: SourceModel){
// 		return this.httpClient.post<SourceModel>(this.baseurl + '/sources', source).pipe(
// 			catchError(this.errorHandl));
// 	}
	loginUser(): Observable<string> {
		// sessionStorage.setItem('type','facebook');
		return this.httpClient.get<string>(this.baseurl + 'Authentication/Social/Open?type=1').pipe(
			catchError(this.errorHandl));
	}

	valami() {
		let token = sessionStorage.getItem('t');
		// let type = sessionStorage.getItem('type');
		let options = {headers: {'Authorization': 'Bearer ' + token/*, 'Type': type*/}};
		return this.httpClient.get<string>(this.baseurl + 'valami', options).pipe(catchError(this.errorHandl));
	}

	// passURL(url: string): Observable<string>{
	// 	return this.httpClient.post<string>(this.baseurl + 'auth', '"'+url+'"',this.httpOptions).pipe(
	// 		catchError(this.errorHandl));
	// }

	logoutUser(): Observable<boolean> {
		let token = sessionStorage.getItem('t');
		let options = {headers: {'Authorization': 'Bearer ' + token}};
		return this.httpClient.get<boolean>(this.baseurl + 'logout', options).pipe(catchError(this.errorHandl));
	}

	registerUser(user: RegisterModel) {
		return this.httpClient.post<RegisterModel>(this.baseurl + 'Authentication/register', user).pipe(
			catchError(this.errorHandl));
	}
	registerSocialUser(user: RegisterModel) {
		return this.httpClient.post<RegisterModel>(this.baseurl + 'Authentication/register', user).pipe(
			catchError(this.errorHandl));
	}

	create(user: UsersModel){
		return this.httpClient.post<UsersModel>(this.baseurl + 'User', user).pipe(
			catchError(this.errorHandl));
	}


	register(): Observable<string> {
		return this.httpClient.get<string>(this.baseurl+ 'Authentication/social/open?type=4').pipe(
			catchError(this.errorHandl));
	}

	//GetAllUser
	getAllUser(): Observable<UsersModel[]>{
		let token = sessionStorage.getItem('t');
		let options = {headers: {'Authorization': 'Bearer ' + token/*, 'Type': type*/}};
		return this.httpClient.get<UsersModel[]>(this.baseurl+'user/getAllUsers',options).pipe(
			catchError(this.errorHandl));
	}

// GET
// 	getProjects(request: PageRequest): Observable<PageResponse<ProjectModel>> {
// 		const options = {
// 			params: new HttpParams()
// 				.set('page', request.page.toString())
// 				.set('page_size', request.page_size.toString())
// 				.set('filters', request.filters.toString())
// 				.set('filterValue', request.filterValue.toString())
// 				.set('sortName', request.sortName.toString())
// 				.set('sortDir', request.sortDir.toString())
// 		};
//
// 		return this.httpClient.get<PageResponse<ProjectModel>>(this.baseurl + '/projects', options).pipe(
// 			catchError(this.errorHandl));
// 	}


	// DELETE
	// deleteProject(id: number){
	// 	const options = {
	// 		params: new HttpParams()
	// 			.set('id', id.toString())
	// 	};
	// 	return this.httpClient.delete(this.baseurl + '/projects', options).pipe(
	// 		catchError(this.errorHandl));
	// }


// Error handling
	errorHandl(error) {
		let errorMessage = '';
		if (error.error instanceof ErrorEvent) {
// Get client-side error
			errorMessage = error.error.message;
		} else {
// Get server-side error
			errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
		}
		console.log(errorMessage);
		return throwError(errorMessage);
	}
}
