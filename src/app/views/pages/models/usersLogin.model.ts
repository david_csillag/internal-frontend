export class UsersLoginModel {
	username: string;
	password: string;
	rePass: string;

	constructor(username: string, password: string, rePass: string){
		this.username = username;
		this.password = password;
		this.rePass = rePass;
	}
}
