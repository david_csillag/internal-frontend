import {CountryModel} from './country.model';

export class CityModel {
	public name: string;
	public country_id: CountryModel;

	constructor(name, country_id){
		this.name = name;
		this.country_id = country_id;
	}
}
