export class PageResponse<T> {
	//A válaszban kapott elemek
	items: T[];
	//Az összes oldal száma
	totalPages: number;
	//Az összes elem száma
	length: number;
	//Az oldal mérete
	pageSize: number;
}
