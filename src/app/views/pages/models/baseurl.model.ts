export class BaseurlModel {
	baseurl: string;
	prod: boolean;


	constructor(prod: boolean) {
		this.prod = prod;
		if(this.prod){
			this.baseurl = 'http://10.10.1.25:8990'
		}else this.baseurl = 'https://localhost:5001/api/';

	}
}
