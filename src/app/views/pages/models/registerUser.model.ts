export class RegisterUserModel {
	id: number;
	email: string;
	firstName: string;
	lastName: string;
	access_token: string;

	constructor(id: number, email: string, first_name: string, last_name: string, access_token: string){
		this.id=id;
		this.email=email;
		this.firstName=first_name;
		this.lastName=last_name;
		this.access_token=access_token;
	}
}
