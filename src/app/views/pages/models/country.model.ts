export class CountryModel {
	public name: string;
	public code: string;

	constructor(name, code){
		this.name = name;
		this.code = code;
	}
}
