//Ez az osztály felel azért, hogy az oldal egyik fő elemeként szolgáló táblázat
// oldalkra bontva kapja meg az adatokat
export class PageRequest {
	page: number;
	page_size: number;
	//A keresőszó változója
	filterValue: String;
	//Az eltárolt keresőszavak változója
	filters: Array<String>;
	//A kulcsszavak változója
	tags: Array<String>;
	//A rendezés elnevezése
	sortName: string;
	//A rendezés iránya
	sortDir: string;

	constructor(page: number, page_size: number, filters: Array<String>, tags: Array<String>, filterValue: String,
				sortName: string, sortDir: string) {
		this.page = page;
		this.page_size = page_size;
		this.filters = filters;
		this.tags = tags;
		this.filterValue = filterValue
		this.sortName = sortName;
		this.sortDir = sortDir;
	}

	copy(): PageRequest {
		return <PageRequest>{
			page: this.page,
			page_size: this.page_size,
			filters: this.filters,
			tags: this.tags,
			filterValue: this.filterValue,
			sortName: this.sortName,
			sortDir: this.sortDir
		};
	}
}
