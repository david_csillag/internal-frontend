import {CityModel} from './city.model';

export class ProjectModel {
	public name: string;
	public description: string;
	public city_id: CityModel;

	constructor(name, description, city_id){
		this.name = name;
		this.description = description;
		this.city_id = city_id;
	}
}
