import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatPaginatorIntl} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatAutocomplete, MatAutocompleteSelectedEvent, MatChipInputEvent, MatDialog, MatSort} from '@angular/material';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {SelectionModel} from '@angular/cdk/collections';
import {map, startWith,} from 'rxjs/operators';
import {FormControl} from '@angular/forms';
import {BehaviorSubject, Observable, Subscription} from 'rxjs';
import {COMMA, ENTER, SEMICOLON} from '@angular/cdk/keycodes';
import {ServersService} from '../../services/servers.service';
import {CountryDialogComponent} from '../country-dialog/country-dialog.component';
// import {CountryModel} from '../../models/country.model';
import {KeycloakService} from 'keycloak-angular';

/**
 * @title Table with pagination
 */
@Component({
	selector: 'country-sorting',
	styleUrls: ['country-sorting.component.scss'],
	templateUrl: 'country-sorting.component.html',
	animations: [
		trigger('detailExpand', [
			state('collapsed', style({height: '0px', minHeight: '0'})),
			state('expanded', style({height: '*'})),
			transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
		]),
	],
})
export class CountrySortingComponent implements OnInit, OnDestroy {
	displayedColumns: string[] = ['select', 'name', 'code', 'actions', /*'new',*/];
	newColumns: string[] = ['select', 'Név', 'Kód', 'actions', /*'new',*/ ];
	// dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
	// dataSource: MatTableDataSource<CountryModel> = new MatTableDataSource<CountryModel>();
	subscription: Subscription;
	// expandedElement: CountryModel | null;
	// selection = new SelectionModel<CountryModel>(true, []);
	pageIndex: number;
	filterValue: string;
	visible = true;
	selectable = true;
	removable = true;
	addOnBlur = true;
	separatorKeysCodes: number[] = [ENTER, COMMA, SEMICOLON];
	tagCtrl = new FormControl();
	filteredTags: Observable<string[]>;
	tags: string[] = [];
	allTags: string[] = [];
	ids: number[];
	sub: Subscription;
	onChange$ = new BehaviorSubject<any>(null);
// MatPaginator
	pageSizeOptions: number[] = [5, 10, 20, 30, 50, 100];
	filters: string[] = [];
	editMode = false;
	// @ts-ignore
	newCountry: CountryModel;
	// User: UsersModel;

	@ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
	@ViewChild(MatSort, {static: true}) sort: MatSort;

	@ViewChild('tagInput', {static: false}) tagInput: ElementRef<HTMLInputElement>;
	@ViewChild('auto', {static: false}) matAutocomplete: MatAutocomplete;

	@ViewChild('hidden', {static: false}) hidden: boolean;


	ngOnInit() {
		this.ids = [];
		this.filterValue = '';
		this.paginator.pageSizeOptions = this.pageSizeOptions;
		this.paginator.pageSize = pageSize;
		this.paginator.pageIndex=pageIndex;
		// PAGINATOR FORDÍTÁS **** START ****
		const paginatorIntl = new MatPaginatorIntl();
		paginatorIntl.itemsPerPageLabel = 'Displayed:';
		this.paginator._intl.itemsPerPageLabel = paginatorIntl.itemsPerPageLabel;
		paginatorIntl.nextPageLabel = 'Next page';
		this.paginator._intl.nextPageLabel = paginatorIntl.nextPageLabel;
		paginatorIntl.previousPageLabel = 'Previous pge';
		this.paginator._intl.previousPageLabel = paginatorIntl.previousPageLabel;
		paginatorIntl.firstPageLabel = 'First page';
		this.paginator._intl.firstPageLabel = paginatorIntl.firstPageLabel;
		paginatorIntl.lastPageLabel = 'Last page';
		this.paginator._intl.lastPageLabel = paginatorIntl.lastPageLabel;
		// PAGINATOR FORDÍTÁS **** END ****
		this.sort.active='name';
		this.sort.direction='';

		// merge(this.paginator.page, this.onChange$)
		// 	.pipe(
		// 		debounceTime(300),
		// 		switchMap(() => this.serverService.getCountries(
		// 			new PageRequest(this.paginator.pageIndex, this.paginator.pageSize, this.filters, this.tags,
		// 				this.filterValue, this.sort.active, this.sort.direction))),
		// 		map(data => {
		// 			this.paginator.length = data.length;
		// 			return data.items;
		// 		})
		// 	).subscribe(data => {
		// 		pageSize=this.paginator.pageSize;
		// 		pageIndex=this.paginator.pageIndex;
		// 	this.dataSource.data = data;
		// 	let countryIds: number[] = [];
		// 	for (let country of this.dataSource.data) {
		// 		countryIds.push(country.id);
		// 	}
		// 	this.serverService.getUsedCountry(countryIds).subscribe(data => {
		// 		this.ids = data;
		// 	});
		// 	// console.log(this.dataSource.data);
		// });


		// console.log(this.dataSource);
	}

	constructor(public dialog: MatDialog, private serverService: ServersService, private keycloak: KeycloakService) {
		// console.log(keycloak.getUserRoles());
		// this.User = new UsersModel(-1,this.keycloak.getKeycloakInstance().tokenParsed['sub'],this.keycloak.getKeycloakInstance().tokenParsed['name'], this.keycloak.getKeycloakInstance().tokenParsed['email']);
		this.filteredTags = this.tagCtrl.valueChanges.pipe(
			startWith(''),
			map((tag: string | null) => tag ? this._filter(tag) : []));
	}

	isAllSelected() {
		// const numSelected = this.selection.selected.length;
		// let num = 0;
		// for (let i = 0; i < this.dataSource.data.length; i++) {
		// 	if (this.ids[i]===0) {
		// 		num += 1;
		// 	}
		// }
		// return numSelected === num;
	}

	masterToggle() {
		// this.isAllSelected() ?
		// 	this.selection.clear() :
		// 	this.dataSource._orderData(this.dataSource.filteredData)
		// 		.slice()
		// 		.forEach(row => this.ids[this.dataSource.data.indexOf(row)]===0 ? this.selection.select(row) : null);
	}

	// checkboxLabel(row?: CountryModel): string {
	// 	if (!row) {
	// 		return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
	// 	}
	// 	return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.name}`;
	// }

	pageChange() {
		// this.selection.clear();
	}

	add(event: MatChipInputEvent): void {
		// Add fruit only when MatAutocomplete is not open
		// To make sure this does not conflict with OptionSelected Event

		const value = event.value;
		if ((value || '').trim()) {
			if (!this.filters.find(f => f === value)) {
				this.filters.push((value));
				this.applyFilter(this.filterValue);
			}
		}
		this.tagCtrl.setValue(null);
		this.paginator.firstPage();
		this.onChange$.next(null);

	}

	remove(tag: string): void {
		const index = this.filters.indexOf(tag);

		if (index >= 0) {
			this.filters.splice(index, 1);
			this.applyFilter(this.filterValue);
		} else {
			this.applyFilter(this.filterValue);
		}
		// console.log(this.tags);
		this.paginator.firstPage();
		this.onChange$.next(null);
	}

	selected(event: MatAutocompleteSelectedEvent): void {
		if (!this.filters.includes(event.option.viewValue)) {
			this.filters.push((event.option.viewValue));
		}
		this.applyFilter(this.filterValue);
		this.tagInput.nativeElement.value = '';
		this.filterValue='';
		this.tagCtrl.setValue(null);
		// console.log(this.tags);
		this.paginator.firstPage();
		this.onChange$.next(null);
	}

	public applyFilter(filterValue?: string) {
		// if (this.selection.hasValue()) {
		// 	this.selection.clear();
		// }
		this.filterValue = filterValue.toLowerCase();
		this.paginator.firstPage();
		this.onChange$.next(null);
		// this.dataSource.filter = ' ';
	}

	//legördülő lista előállítása
	private _filter(value: string): string[] {
		const filterValue = value.toLowerCase();
		//chips hossza > n then legördülő lista
		this.applyFilter(this.filterValue);
		this.paginator.firstPage();
		this.onChange$.next(null);
		return this.allTags.filter(tag => (!this.tags.includes(tag)) && (tag.toLowerCase().indexOf(filterValue) === 0));
	}

	actRow(/*row?: CountryModel*/) {
		// if (this.selection.isSelected(row)) {
		// 	// this.dataSource.data.splice(this.dataSource.data.indexOf(row), 1);
		// 	this.selection.deselect(row);
		// 	// this.serverService.deleteCountry(row.id).subscribe();
		// 	this.paginator.firstPage();
		// 	this.onChange$.next(null);
		// }
	}

	clearAll() {
		// for (let select of this.selection.selected) {
		// 	console.log(select);
		// 	// this.serverService.deleteCountry(select.id).subscribe();
		// }
		// this.selection.clear();
		// this.paginator.firstPage();
		// this.onChange$.next(null);
	}

	openDialog(/*row?: CountryModel | null*/): void {

		// if (row === null) {
		// 	// @ts-ignore
		// 	this.newCountry = new CountryModel('', '', null, null);
		// } else {
		// 	this.editMode = true;
		// 	this.newCountry = row;
		// }
		// const dialogRef = this.dialog.open(CountryDialogComponent, {
		// 	width: '65%',
		// 	height: '45%',
		// 	disableClose: true,
		// 	data: this.newCountry,
		// });

		// dialogRef.afterClosed().subscribe(result => {
		// 	// console.log('The dialog was closed');
		// 	if (result) {
		// 		this.paginator.firstPage();
		// 		this.onChange$.next(null);
		// 	}
		// });
	}

	sortData(event){
		console.log(event);
		this.onChange$.next(null);
	}

	ngOnDestroy() {
	}
}
export let pageSize: number = 5;
export let pageIndex: number = 0;
