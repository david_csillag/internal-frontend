import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatPaginatorIntl} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatAutocomplete, MatAutocompleteSelectedEvent, MatChipInputEvent, MatDialog, MatSort} from '@angular/material';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {SelectionModel} from '@angular/cdk/collections';
import {map, startWith,} from 'rxjs/operators';
import {FormControl} from '@angular/forms';
import {BehaviorSubject, Observable, Subscription} from 'rxjs';
import {COMMA, ENTER, SEMICOLON} from '@angular/cdk/keycodes';
import {ServersService} from '../../services/servers.service';
import {DialogComponent} from '../dialog/dialog.component';
// import {CityModel} from '../../models/city.model';
import {KeycloakService} from 'keycloak-angular';

/**
 * @title Table with pagination
 */
@Component({
	selector: 'city-sorting',
	styleUrls: ['city-sorting.component.scss'],
	templateUrl: 'city-sorting.component.html',
	animations: [
		trigger('detailExpand', [
			state('collapsed', style({height: '0px', minHeight: '0'})),
			state('expanded', style({height: '*'})),
			transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
		]),
	],
})
export class CitySortingComponent implements OnInit, OnDestroy {
	//A megjelenített oszlopok
	displayedColumns: string[] = ['select', 'name', 'countryId', 'actions', /*'new',*/];
	//Ugyanazok az oszlopok csak magyarul
	newColumns: string[] = ['select', 'Név', 'Ország', 'actions', /*'new',*/];
	//A városokat tartalmazó adathalmaz
	// dataSource: MatTableDataSource<CityModel> = new MatTableDataSource<CityModel>();
	subscription: Subscription;
	// expandedElement: CityModel | null;
	//A bepipált elemek listája
	// selection = new SelectionModel<CityModel>(true, []);
	//A keresőbe begépelt szó
	filterValue: string;
	visible = true;
	selectable = true;
	removable = true;
	addOnBlur = true;
	separatorKeysCodes: number[] = [ENTER, COMMA, SEMICOLON];
	tagCtrl = new FormControl();
	//A tárolt keresőszavak
	filteredTags: Observable<string[]>;
	tags: string[] = [];
	allTags: string[] = [];
	ids: number[];
	sub: Subscription;
	onChange$ = new BehaviorSubject<any>(null);
// MatPaginator
	pageSizeOptions: number[] = [5, 10, 20, 30, 50, 100];
	filters: string[] = [];
	editMode = false;
	//A szerkesztésre vagy létrehozásra átadni kívánt új objektum
	// @ts-ignore
	newCity: CityModel;

	@ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
	@ViewChild(MatSort, {static: true}) sort: MatSort;

	@ViewChild('tagInput', {static: false}) tagInput: ElementRef<HTMLInputElement>;
	@ViewChild('auto', {static: false}) matAutocomplete: MatAutocomplete;

	@ViewChild('hidden', {static: false}) hidden: boolean;


	ngOnInit() {
		this.ids = [];
		this.filterValue = '';
		this.paginator.pageSizeOptions = this.pageSizeOptions;
		this.paginator.pageSize = pageSize;
		this.paginator.pageIndex = pageIndex;
		// PAGINATOR FORDÍTÁS **** START ****
		const paginatorIntl = new MatPaginatorIntl();
		paginatorIntl.itemsPerPageLabel = 'Displayed:';
		this.paginator._intl.itemsPerPageLabel = paginatorIntl.itemsPerPageLabel;
		paginatorIntl.nextPageLabel = 'Next page';
		this.paginator._intl.nextPageLabel = paginatorIntl.nextPageLabel;
		paginatorIntl.previousPageLabel = 'Previous pge';
		this.paginator._intl.previousPageLabel = paginatorIntl.previousPageLabel;
		paginatorIntl.firstPageLabel = 'First page';
		this.paginator._intl.firstPageLabel = paginatorIntl.firstPageLabel;
		paginatorIntl.lastPageLabel = 'Last page';
		this.paginator._intl.lastPageLabel = paginatorIntl.lastPageLabel;
		// PAGINATOR FORDÍTÁS **** END ****
		this.sort.active = 'name';
		this.sort.direction = '';

		// merge(this.paginator.page, this.onChange$)
		// 	.pipe(
		// 		debounceTime(300),
		// 		switchMap(() => this.serverService.getCities(
		// 			new PageRequest(this.paginator.pageIndex, this.paginator.pageSize, this.filters, this.tags,
		// 				this.filterValue, this.sort.active, this.sort.direction))),
		// 		map(data => {
		// 			this.paginator.length = data.length;
		//
		// 			return data.items;
		// 		})
		// 	).subscribe(data => {
		// 		pageSize=this.paginator.pageSize;
		// 		pageIndex=this.paginator.pageIndex;
		// 	this.dataSource.data = data;
		// 	let cityIds: number[]=[];
		// 	for (let city of this.dataSource.data) {
		// 		cityIds.push(city.id);
		// 	}
		// 	this.serverService.getUsedCity(cityIds).subscribe(data => {
		// 		this.ids=data;
		// 	});
		// });


	}

	constructor(public dialog: MatDialog, private serverService: ServersService, private keycloak: KeycloakService) {
		this.filteredTags = this.tagCtrl.valueChanges.pipe(
			startWith(''),
			map((tag: string | null) => tag ? this._filter(tag) : []));
	}

	//Ez a metódus mondja meg, hogy a lapon látható összes elem ki van-e jelölve
	isAllSelected() {
		// const numSelected = this.selection.selected.length;
		// let num = 0;
		// for (let i = 0; i < this.dataSource.data.length; i++) {
		// 	if (this.ids[i]===0) {
		// 		num += 1;
		// 	}
		// }
		// return numSelected === num;
	}

	masterToggle() {
		// this.isAllSelected() ?
		// 	this.selection.clear() :
		// 	this.dataSource._orderData(this.dataSource.filteredData)
		// 		.slice()
		// 		.forEach(row => this.ids[this.dataSource.data.indexOf(row)]===0 ? this.selection.select(row) : null);
	}

	//Ez a metódus felel azért, hogy egyesével ki lehessen jelölni elemeket
	checkboxLabel(/*row?: CityModel*/)/*: string */{
		// if (!row) {
		// 	return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
		// }
		// return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.name}`;
	}

	//Ez a metódus oldalváltásnál törli a szelekiót
	pageChange() {
		// this.selection.clear();
	}

	//Ez a metódus tárol kulcs és keresőszavakat
	add(event: MatChipInputEvent): void {
		// Add city only when MatAutocomplete is not open
		// To make sure this does not conflict with OptionSelected Event

		const value = event.value;
		if ((value || '').trim()) {
			if (!this.filters.find(f => f === value)) {
				this.filters.push((value));
				this.applyFilter(this.filterValue);
			}
		}
		this.tagCtrl.setValue(null);
		this.paginator.firstPage();
		this.onChange$.next(null);

	}

	//Ez a metódus felel azért, hogy egy tárolt chip törölve legyen
	remove(tag: string): void {
		const index = this.filters.indexOf(tag);

		if (index >= 0) {
			this.filters.splice(index, 1);
			this.applyFilter(this.filterValue);
		} else {
			this.applyFilter(this.filterValue);
		}
		// console.log(this.tags);
		this.paginator.firstPage();
		//Ez a hívás frissíti a megjelenítendő adathalmazt
		this.onChange$.next(null);
	}

	//Ez a metódus felel azért, hogy az Autocomplete-ból kiválasztott elem tárolásra kerüljön
	selected(event: MatAutocompleteSelectedEvent): void {
		if (!this.filters.includes(event.option.viewValue)) {
			this.filters.push((event.option.viewValue));
		}
		this.applyFilter(this.filterValue);
		this.tagInput.nativeElement.value = '';
		this.filterValue = '';
		this.tagCtrl.setValue(null);
		// console.log(this.tags);
		this.paginator.firstPage();
		this.onChange$.next(null);
	}

	//Ez a metódus felel azért, hogy a begépelt keresőszó frissítse a megjelenített adathalamzt
	public applyFilter(filterValue?: string) {
		// if (this.selection.hasValue()) {
		// 	this.selection.clear();
		// }
		// this.filterValue = filterValue.toLowerCase();
		// this.paginator.firstPage();
		// this.onChange$.next(null);
		// this.dataSource.filter = ' ';
	}

	//legördülő lista előállítása
	private _filter(value: string): string[] {
		const filterValue = value.toLowerCase();
		//chips hossza > n then legördülő lista
		this.applyFilter(this.filterValue);
		this.paginator.firstPage();
		this.onChange$.next(null);
		return this.allTags.filter(tag => (!this.tags.includes(tag)) && (tag.toLowerCase().indexOf(filterValue) === 0));
	}

	//Ez a metódus felel azért, hogy a megfelelő város kerüljön törlésre
	actRow(/*row?: CityModel*/) {
		// if (this.selection.isSelected(row)) {
		// 	// this.dataSource.data.splice(this.dataSource.data.indexOf(row), 1);
		// 	this.selection.deselect(row);
		// 	// this.serverService.deleteCity(row.id).subscribe();
		// 	this.paginator.firstPage();
		// 	this.onChange$.next(null);
		// }
	}

	//Ez a metódus felel azért, hogy minden kijelölt elem törtlésre kerüljön
	clearAll() {
		// for (let select of this.selection.selected) {
		// 	console.log(select);
		// 	// this.serverService.deleteCity(select.id).subscribe();
		// }
		// this.selection.clear();
		// this.paginator.firstPage();
		// this.onChange$.next(null);
	}

	//Ez a metódus flel azért, hogy a dialógus ablak a megfelelő adatokkal kerüljön megnyitásra
	openDialog(/*row?: CityModel | null*/): void {
		//Amennyiben nem egy létező elem módosítására mentünk, úgy a row null értéket kap, ilyenkor kizárásos alapon új elemet akarunk létrehozni
		// if (row === null) {
		// 	//a created_at paramétert itt adjuk át neki, hogy az később biztosan meglegyen
		// 	// @ts-ignore
		// 	this.newCity = new CityModel('', '', null, null);
		// } else {
		// 	this.editMode = true;
		// 	this.newCity = row;
		// }
		// //A dialógusablak megyitása az ahhoz használt paraméterekkel
		// const dialogRef = this.dialog.open(DialogComponent, {
		// 	width: '65%',
		// 	height: '45%',
		// 	disableClose: true,
		// 	data: this.newCity,
		// });

		//A dialógusablak bezárásakor történő műveletek
		// dialogRef.afterClosed().subscribe(result => {
		// 	// console.log('The dialog was closed');
		// 	if (result) {
		// 		this.paginator.firstPage();
		// 		this.onChange$.next(null);
		// 	}
		// });
	}

	//Ez a függvény felel az adatok rendezéséért
	sortData(event) {
		console.log(event);
		this.onChange$.next(null);
	}

	ngOnDestroy() {
	}
}
export let pageSize: number = 5;
export let pageIndex: number = 0;
