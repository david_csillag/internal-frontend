import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CitySortingComponent} from './city-sorting.component';

describe('TableSortingComponent', () => {
  let component: CitySortingComponent;
  let fixture: ComponentFixture<CitySortingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CitySortingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CitySortingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
