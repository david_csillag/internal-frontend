import {Component, ElementRef, Inject, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatAutocomplete, MatPaginator, MatSnackBar, MatSort} from '@angular/material';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {COMMA, ENTER, SEMICOLON} from '@angular/cdk/keycodes';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Observable, Subscription} from 'rxjs';
import {ServersService} from '../../services/servers.service';
// import {CountryModel} from '../../models/country.model';
import {KeycloakService} from 'keycloak-angular';

@Component({
	selector: 'kt-dialog',
	templateUrl: './country-dialog.component.html',
	styleUrls: ['./country-dialog.component.scss']
})
export class CountryDialogComponent implements OnInit, OnDestroy {
	visible = true;
	selectable = true;
	removable = true;
	addOnBlur = true;
	editMode = false;
	separatorKeysCodes: number[] = [ENTER, COMMA, SEMICOLON];
	tagCtrl = new FormControl();
	tags: string[] = [];
	dataColumn: string[] = ['name', 'code'];
	options: string[] = [];
	filteredTagOptions: Observable<string[]>;
	// allCountries: CountryModel[] = [];
	sub: Subscription;
	subs: Subscription;
	controlForm: FormGroup;
	filterValue;
	message: string;
	// User: UsersModel;


	@ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
	@ViewChild(MatSort, {static: true}) sort: MatSort;
	@ViewChild('tagInput', {static: false}) tagInput: ElementRef<HTMLInputElement>;
	@ViewChild('auto', {static: false}) matAutocomplete: MatAutocomplete;
	@ViewChild('autos', {static: false}) matTagAutocomplete: MatAutocomplete;
	@ViewChild('hidden', {static: false}) hidden: boolean;
	@ViewChild('pubInput', {static: false}) pubInput: string;

	constructor(
		public dialogRef: MatDialogRef<CountryDialogComponent>,
		// @Inject(MAT_DIALOG_DATA) public data: CountryModel,
		private serverService: ServersService,
		private fb: FormBuilder, public snackBar: MatSnackBar, private keycloak: KeycloakService) {
		this.filterValue = '';
		// this.User = new UsersModel(-1, this.keycloak.getKeycloakInstance().tokenParsed['sub'], this.keycloak.getKeycloakInstance().tokenParsed['name'], this.keycloak.getKeycloakInstance().tokenParsed['email']);
		// if(data){
		// 	data.modified_by = this.User;
		// }else {
		// 	data.created_by = this.User;
		// 	data.modified_by = this.User;
		// }
	}

	ngOnInit(): void {
		this.filterValue = '';
		this.initForm();
		// if(this.data.code.trim().length===0){
		// 	this.editMode = false;
		// 	console.log(this.editMode);
		// 	this.message = 'Country created successfully!'
		// }else{
		// 	this.editMode = true;
		// 	console.log(this.editMode);
		// 	this.message = 'Country edited successfully!'
		// }


		// this.subs = this.serverService.getAllCountries().subscribe(
		// 	data => {
		// 		this.allCountries = data;
		// 		if(data){
		// 		this.allCountries.forEach(f => this.options.push(f.name));
		// 		}
		// 		console.log(this.options);
		// 		console.log('GET Countries Request successful!', data);
		// 	}
		// );
	}


	onNoClick(): void {
		this.dialogRef.close();
	}

	private initForm() {
		let id: number;
		let name: string;
		let code: string;


		// const country = this.data;
		// id = country.id;
		// name = country.name;
		// code = country.code;

		this.controlForm = new FormGroup({
			// id: new FormControl(country.id),
			name: new FormControl(name, Validators.required),
			code: new FormControl(code, Validators.required),
			// created_by: new FormControl(this.data.created_by),
			// modified_by: new FormControl(this.data.modified_by)
		});
	}

	log(){
		console.log(this.controlForm.value);
	}

	onSubmit() {
		// if (this.editMode && !this.compare()) {
		// 	console.log(this.editMode);
		// 	// this.serverService.putCountry(this.controlForm.value).subscribe();
		// } else if(!this.editMode) {
		// 	// console.log(this.controlForm.controls['created_by'].value);
		// 	// this.serverService.createCountry(this.controlForm.value).subscribe();
		// }
		this.dialogRef.close('result');
	}
	compare()/*: boolean*/{
		// return (this.data.name === this.controlForm.controls['name'].value);
	}

	openSnackBar(message: string, action: string) {
		this.snackBar.open(message, action,{
			duration: 2000,
		});
	}

	ngOnDestroy(): void {
	}
}
