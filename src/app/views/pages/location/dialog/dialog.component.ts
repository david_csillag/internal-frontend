import {Component, ElementRef, Inject, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatAutocomplete, MatPaginator, MatSnackBar, MatSort} from '@angular/material';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {COMMA, ENTER, SEMICOLON} from '@angular/cdk/keycodes';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Observable, Subscription} from 'rxjs';
import {ServersService} from '../../services/servers.service';


import {KeycloakService} from 'keycloak-angular';

@Component({
	selector: 'kt-dialog',
	templateUrl: './dialog.component.html',
	styleUrls: ['./dialog.component.scss']
})
export class DialogComponent implements OnInit, OnDestroy {
	visible = true;
	selectable = true;
	removable = true;
	addOnBlur = true;
	editMode = false;
	separatorKeysCodes: number[] = [ENTER, COMMA, SEMICOLON];
	tagCtrl = new FormControl();
	tags: string[] = [];
	dataColumn: string[] = ['name', 'country'];
	options: string[] = [];
	filteredTagOptions: Observable<string[]>;
	// allCountries: CountryModel[] = [];
	sub: Subscription;
	subs: Subscription;
	controlForm: FormGroup;
	filterValue;
	message: string;
	// User: UsersModel;

	@ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
	@ViewChild(MatSort, {static: true}) sort: MatSort;
	@ViewChild('tagInput', {static: false}) tagInput: ElementRef<HTMLInputElement>;
	@ViewChild('auto', {static: false}) matAutocomplete: MatAutocomplete;
	@ViewChild('autos', {static: false}) matTagAutocomplete: MatAutocomplete;
	@ViewChild('hidden', {static: false}) hidden: boolean;
	@ViewChild('pubInput', {static: false}) pubInput: string;

	constructor(
		public dialogRef: MatDialogRef<DialogComponent>,
		// @Inject(MAT_DIALOG_DATA) public data: CityModel,
		private serverService: ServersService,
		private fb: FormBuilder, public snackBar: MatSnackBar, private keycloak: KeycloakService) {

		this.filterValue = '';
		// this.User = new UsersModel(-1, this.keycloak.getKeycloakInstance().tokenParsed['sub'], this.keycloak.getKeycloakInstance().tokenParsed['name'], this.keycloak.getKeycloakInstance().tokenParsed['email']);
		// data.modified_by = this.User;

	}

	// CompareCity(cmp: CityModel, cmpt: CityModel){
	// 	return cmp.id === cmpt.id;
	// }

	ngOnInit(): void {
		this.filterValue = '';
		this.initForm();
		// if(this.data.id<0){
		// 	this.editMode = false;
		// 	console.log(this.editMode);
		// 	this.message = 'City created successfully!'
		// }else{
		// 	this.editMode = true;
		// 	console.log(this.editMode);
		// 	this.message = 'City edited successfully!'
		// }
		// this.subs = this.serverService.getAllCountries().subscribe(
		// 	data => {
		// 		this.allCountries = data;
		// 		if(this.allCountries.length>0) {
		// 			this.allCountries.forEach(f => this.options.push(f.name));
		// 		}
		// 		console.log(this.options);
		// 		console.log('GET Countries Request successful!', data);
		// 	}
		// );
	}


	onNoClick(): void {
		this.dialogRef.close();
	}


	private initForm() {
		let id: number;
		let name: string;
		// let countryId: CountryModel;


		// const city = this.data;
		// id = city.id;
		// name = city.name;
		// countryId = city.countryId;

		this.controlForm = new FormGroup({
			// id: new FormControl(city.id),
			// name: new FormControl(name, Validators.compose([Validators.required])),
			// countryId: new FormControl(countryId, Validators.required),
			// created_by: new FormControl(this.data.created_by),
			// modified_by: new FormControl(this.User)
		});
	}

	log(){
		console.log(this.controlForm.value);
	}

	onSubmit() {
		// if (this.editMode && !this.compare()) {
		// 	// this.serverService.putCity(this.controlForm.value).subscribe();
		// } else if(!this.editMode) {
		// 	// console.log(this.controlForm.controls['created_by'].value);
		// 	// this.serverService.createCity(this.controlForm.value).subscribe();
		// }
		this.dialogRef.close('result');
	}
	// compare(): boolean{
	// 	return (this.data.name === this.controlForm.controls['name'].value) &&
	// 		(this.data.countryId.id === this.controlForm.controls['countryId'].value.id);
	// }

	openSnackBar(message: string, action: string) {
		this.snackBar.open(message, action,{
			duration: 2000,
		});
	}

	ngOnDestroy(): void {
	}
}
