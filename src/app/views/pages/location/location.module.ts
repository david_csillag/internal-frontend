// Angular
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
// Core Module
import {CoreModule} from '../../../core/core.module';
import {PartialsModule} from '../../partials/partials.module';
import {LocationComponent} from './location.component';
import {DialogComponent} from './dialog/dialog.component';
import {CitySortingComponent} from './city-sorting/city-sorting.component';
import {
	MatAutocompleteModule,
	MatButtonModule,
	MatCheckboxModule,
	MatChipsModule,
	MatDialogModule,
	MatFormFieldModule,
	MatGridListModule,
	MatIconModule,
	MatInputModule,
	MatPaginatorModule,
	MatSelectModule,
	MatSortModule,
	MatTableModule,
	MatTabsModule
} from '@angular/material';
import {ReactiveFormsModule} from '@angular/forms';
import {CountrySortingComponent} from './country-sorting/country-sorting.component';
import {CountryDialogComponent} from './country-dialog/country-dialog.component';

@NgModule({
	imports: [
		CommonModule,
		PartialsModule,
		CoreModule,
		RouterModule.forChild([
			{
				path: '',
				component: LocationComponent
			},
		]),
		MatTabsModule,
		MatFormFieldModule,
		MatChipsModule,
		MatIconModule,
		MatAutocompleteModule,
		MatCheckboxModule,
		MatPaginatorModule,
		ReactiveFormsModule,
		MatTableModule,
		MatSortModule,
		MatButtonModule,
		MatInputModule,
		MatSelectModule,
		MatDialogModule,
		MatGridListModule,
	],
	providers: [],
	declarations: [
		LocationComponent, DialogComponent, CitySortingComponent, CountrySortingComponent, CountryDialogComponent
	],
	entryComponents:[CitySortingComponent, DialogComponent, CountrySortingComponent, CountryDialogComponent]
})
export class LocationModule {
}
