import {Component, ElementRef, Inject, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatAutocomplete, MatPaginator, MatSnackBar, MatSort} from '@angular/material';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {COMMA, ENTER, SEMICOLON} from '@angular/cdk/keycodes';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Observable, Subscription} from 'rxjs';
import {ServersService} from '../../services/servers.service';

// import {CityModel} from '../../models/city.model';
// import {ProjectModel} from '../../models/project.model';
import {KeycloakService} from 'keycloak-angular';

@Component({
	selector: 'kt-dialog',
	templateUrl: './dialog.component.html',
	styleUrls: ['./dialog.component.scss']
})
export class DialogComponent implements OnInit, OnDestroy {
	visible = true;
	selectable = true;
	removable = true;
	addOnBlur = true;
	editMode = false;
	separatorKeysCodes: number[] = [ENTER, COMMA, SEMICOLON];
	tagCtrl = new FormControl();
	tags: string[] = [];
	dataColumn: string[] = ['name', 'description', 'city'];
	options: string[] = [];
	filteredTagOptions: Observable<string[]>;
	// allCities: CityModel[] = [];
	sub: Subscription;
	subs: Subscription;
	controlForm: FormGroup;
	filterValue;
	message: string;
	// User: UsersModel;


	@ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
	@ViewChild(MatSort, {static: true}) sort: MatSort;
	@ViewChild('tagInput', {static: false}) tagInput: ElementRef<HTMLInputElement>;
	@ViewChild('auto', {static: false}) matAutocomplete: MatAutocomplete;
	@ViewChild('autos', {static: false}) matTagAutocomplete: MatAutocomplete;
	@ViewChild('hidden', {static: false}) hidden: boolean;
	@ViewChild('pubInput', {static: false}) pubInput: string;

	constructor(
		public dialogRef: MatDialogRef<DialogComponent>,
		// @Inject(MAT_DIALOG_DATA) public data: ProjectModel,
		private serverService: ServersService,
		private fb: FormBuilder, public snackBar: MatSnackBar, private keycloak: KeycloakService) {
		// this.User = new UsersModel(-1, this.keycloak.getKeycloakInstance().tokenParsed['sub'],
		// 	this.keycloak.getKeycloakInstance().tokenParsed['name'],this.keycloak.getKeycloakInstance().tokenParsed['email']);
		this.filterValue = '';
		// this.editMode = !!data;

		// if(data){
		// 	this.message = 'Project created successfully!'
		// }else{
		// 	this.message = 'Project edited successfully!'
		// }

	}

	CompareCity(/*cmp: CityModel, cmpt: CityModel*/){
		// return cmp.id === cmpt.id;
	}

	ngOnInit(): void {
		this.filterValue = '';
		// this.editMode = !!this.data.cityId;
		// this.User = new UsersModel(-1, this.keycloak.getKeycloakInstance().tokenParsed['sub'],
		// 	this.keycloak.getKeycloakInstance().tokenParsed['name'],this.keycloak.getKeycloakInstance().tokenParsed['email']);
		this.initForm();

		// this.subs = this.serverService.getAllCities().subscribe(
		// 	data => {
		// 		this.allCities = data;
		// 		if(data) {
		// 			this.allCities.forEach(f => this.options.push(f.name));
		// 		}
		// 		console.log(this.options);
		// 		console.log('GET Publisher Request successful!', data);
		// 	}
		// );
	}


	onNoClick(): void {
		this.dialogRef.close();
	}


	private initForm() {
		let id: number;
		let name: string;
		let description: string;
		// let cityId: CityModel;


		// const project = this.data;
		// id = project.id;
		// name = project.name;
		// description = project.description;
		// cityId = project.cityId;
		// if(this.editMode){
		// 	project.modified_by=this.User;
		// }else {
		// 	project.created_by=this.User;
		// 	project.modified_by=this.User;
		// }

		this.controlForm = new FormGroup({
			// id: new FormControl(project.id),
			// name: new FormControl(name, Validators.required),
			// description: new FormControl(description, Validators.required),
			// cityId: new FormControl(cityId, Validators.required),
			// created_by: new FormControl(project.created_by),
			// modified_by: new FormControl(project.modified_by)
		});
	}

	log(){
		console.log(this.controlForm.value);
	}

	onSubmit() {
		// if (this.editMode && !this.compare()) {
		// 	console.log('Edit mode');
		// 	// this.serverService.putProject(this.controlForm.value).subscribe();
		// } else if(!this.editMode) {
		// 	console.log('Nem edit mode');
		// 	// this.serverService.createProject(this.controlForm.value).subscribe();
		// }
		// this.dialogRef.close('result');
	}
	compare()/*: boolean*/{
		// return (this.data.name === this.controlForm.controls['name'].value) &&
		// 	(this.data.description === this.controlForm.controls['description'].value) &&
		// 	(this.data.cityId.id === this.controlForm.controls['cityId'].value.id);
	}

	openSnackBar(message: string, action: string) {
		this.snackBar.open(message, action,{
			duration: 2000,
		});
	}

	ngOnDestroy(): void {
	}
}
