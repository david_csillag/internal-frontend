import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatPaginatorIntl} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatAutocomplete, MatAutocompleteSelectedEvent, MatChipInputEvent, MatDialog, MatSort} from '@angular/material';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {SelectionModel} from '@angular/cdk/collections';
import {map, startWith,} from 'rxjs/operators';
import {FormControl} from '@angular/forms';
import {BehaviorSubject, Observable, Subscription} from 'rxjs';
import {COMMA, ENTER, SEMICOLON} from '@angular/cdk/keycodes';
// import {ProjectModel} from '../../models/project.model';
import {ServersService} from '../../services/servers.service';
import {DialogComponent} from '../dialog/dialog.component';
import {KeycloakService} from 'keycloak-angular';
import {UsersModel} from '../../models/users.model';

/**
 * @title Table with pagination
 */
@Component({
	selector: 'table-sorting',
	styleUrls: ['table-sorting.component.scss'],
	templateUrl: 'table-sorting.component.html',
	animations: [
		trigger('detailExpand', [
			state('collapsed', style({height: '0px', minHeight: '0'})),
			state('expanded', style({height: '*'})),
			transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
		]),
	],
})
export class TableSortingComponent implements OnInit, OnDestroy {
	displayedColumns: string[] = [/*'select',*/ 'displayName', 'email'/*, 'actions',*/ /*'new',*/];
	newColumns: string[] = [/*'select', */'Név', 'E-mail cím'/*, 'actions',*/ /*'new',*/ ];
	subscription: Subscription;
	userList: UsersModel[];

	addOnBlur = true;
	sub: Subscription;

	ngOnInit() {
		this.serverService.getAllUser().subscribe(f=>this.userList=f);
	}

	constructor(private serverService: ServersService) {
	}

	ngOnDestroy() {
	}
}
