import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'kt-table-content',
  templateUrl: './table-content.component.html',
  styleUrls: ['./table-content.component.scss']
})
export class TableContentComponent implements OnInit {

	@Input()
	element;

  constructor() { }

  ngOnInit() {
  }

}
