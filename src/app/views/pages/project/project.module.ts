// Angular
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
// Core Module
import {CoreModule} from '../../../core/core.module';
import {PartialsModule} from '../../partials/partials.module';
import {ProjectComponent} from './project.component';
import {DialogComponent} from './dialog/dialog.component';
import {TableSortingComponent} from './table-sorting/table-sorting.component';
import {
	MatAutocompleteModule,
	MatButtonModule,
	MatCheckboxModule,
	MatChipsModule,
	MatDialogModule,
	MatDividerModule,
	MatFormFieldModule,
	MatGridListModule,
	MatIconModule,
	MatInputModule,
	MatPaginatorModule,
	MatSelectModule,
	MatSortModule,
	MatTableModule
} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgbRatingModule} from '@ng-bootstrap/ng-bootstrap';
import {TableContentComponent} from './table-sorting/table-content/table-content.component';

@NgModule({
	imports: [
		CommonModule,
		PartialsModule,
		//TableSortingModule,
		CoreModule,
		RouterModule.forChild([
			{
				path: '',
				component: ProjectComponent
			},
		]),
		MatPaginatorModule,
		MatFormFieldModule,
		MatChipsModule,
		MatIconModule,
		ReactiveFormsModule,
		MatAutocompleteModule,
		MatButtonModule,
		MatTableModule,
		MatSortModule,
		MatCheckboxModule,
		MatDialogModule,
		MatInputModule,
		FormsModule,
		MatSelectModule,
		NgbRatingModule,
		MatDividerModule,
		MatGridListModule,
	],
	providers: [],
	declarations: [
		ProjectComponent, DialogComponent, TableSortingComponent, TableContentComponent
	],
	entryComponents:[TableSortingComponent, DialogComponent]
})
export class ProjectModule {
}
