// Angular
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
// Core Module
import {CoreModule} from '../../../core/core.module';
import {PartialsModule} from '../../partials/partials.module';
import {SourceComponent} from './source.component';
import {TableSortingComponent} from './table-sorting/table-sorting.component';
import {DialogComponent} from './dialog/dialog.component';
import {
	MatAutocompleteModule,
	MatButtonModule,
	MatCheckboxModule,
	MatChipsModule,
	MatFormFieldModule,
	MatGridListModule,
	MatIconModule,
	MatInputModule,
	MatPaginatorModule,
	MatSelectModule,
	MatSortModule,
	MatTableModule
} from '@angular/material';
import {MatDialogModule} from '@angular/material/dialog';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {RateDialogComponent} from './rate-dialog/rate-dialog.component';
import {TableContentComponent} from './table-sorting/table-content/table-content.component';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {MatSelectFilterModule} from 'mat-select-filter';
import {RatingContentComponent} from './table-sorting/rating-content/rating-content.component';

@NgModule({
	imports: [
		CommonModule,
		PartialsModule,
		//TableSortingModule,
		CoreModule,
		RouterModule.forChild([
			{
				path: '',
				component: SourceComponent
			},
		]),
		MatPaginatorModule,
		MatFormFieldModule,
		MatChipsModule,
		MatIconModule,
		ReactiveFormsModule,
		MatAutocompleteModule,
		MatButtonModule,
		MatTableModule,
		MatSortModule,
		MatCheckboxModule,
		MatDialogModule,
		MatInputModule,
		FormsModule,
		MatSelectModule,
		NgbModule,
		ScrollingModule,
		MatGridListModule,
		MatSelectFilterModule,
	],
	providers: [],
	declarations: [
		SourceComponent,
		TableSortingComponent,
		DialogComponent,
		RateDialogComponent,
		TableContentComponent,
		RatingContentComponent,
	],
	entryComponents: [TableSortingComponent, DialogComponent, RateDialogComponent],
})

export class SourceModule {
}
