import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatPaginatorIntl} from '@angular/material';
import {BehaviorSubject} from 'rxjs';
// import {RatingModel} from '../../../models/rating.model';
import {ServersService} from '../../../services/servers.service';

@Component({
	selector: 'kt-rating-content',
	templateUrl: './rating-content.component.html',
	styleUrls: ['./rating-content.component.scss']
})
export class RatingContentComponent implements OnInit {


	@Input()
	element;

	@ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

	onChange$ = new BehaviorSubject<any>(null);
	// ratings: RatingModel[] = [];

	constructor(private serverService: ServersService) {
	}

	ngOnInit() {
		this.paginator.pageSize = pageSize;
		this.paginator.pageIndex = pageIndex;
		// PAGINATOR FORDÍTÁS **** START ****
		const paginatorIntl = new MatPaginatorIntl();
		paginatorIntl.itemsPerPageLabel = 'Displayed:';
		this.paginator._intl.itemsPerPageLabel = paginatorIntl.itemsPerPageLabel;
		paginatorIntl.nextPageLabel = 'Next page';
		this.paginator._intl.nextPageLabel = paginatorIntl.nextPageLabel;
		paginatorIntl.previousPageLabel = 'Previous pge';
		this.paginator._intl.previousPageLabel = paginatorIntl.previousPageLabel;
		paginatorIntl.firstPageLabel = 'First page';
		this.paginator._intl.firstPageLabel = paginatorIntl.firstPageLabel;
		paginatorIntl.lastPageLabel = 'Last page';
		this.paginator._intl.lastPageLabel = paginatorIntl.lastPageLabel;
		// PAGINATOR FORDÍTÁS **** END ****

		// merge(this.paginator.page, this.onChange$)
		// 	.pipe(
		// 		debounceTime(300),
		// 		switchMap(() => this.serverService.getRatings(
		// 			this.element.id, pageSize, this.paginator.pageIndex)),
		// 		map(data => {
		// 			this.paginator.length = data.length;
		// 			return data.items;
		// 		})
		// 	).subscribe(data => {
		// 	pageSize = this.paginator.pageSize;
		// 	pageIndex = this.paginator.pageIndex;
		// 	this.ratings = data;
		// });
	}

}

export let pageIndex: number = 0;
export let pageSize: number = 4;
