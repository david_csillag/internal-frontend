import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatPaginatorIntl} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatAutocomplete, MatAutocompleteSelectedEvent, MatChipInputEvent, MatSort} from '@angular/material';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {SelectionModel} from '@angular/cdk/collections';
import {map, startWith,} from 'rxjs/operators';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {BehaviorSubject, Observable} from 'rxjs';
import {COMMA, ENTER, SEMICOLON} from '@angular/cdk/keycodes';
// import {SourceModel} from '../../models/source.model';
import {DialogComponent} from '../dialog/dialog.component';
import {MatDialog} from '@angular/material/dialog';
import {ServersService} from '../../services/servers.service';
// import {TagModel} from '../../models/tag.model';
// import {RatingModel} from '../../models/rating.model';
import {KeycloakService} from 'keycloak-angular';
import {RateDialogComponent} from '../rate-dialog/rate-dialog.component';
import {UsersModel} from '../../models/users.model';

/**
 * @title Table with pagination
 */
@Component({
	selector: 'table-sorting',
	styleUrls: ['table-sorting.component.scss'],
	templateUrl: 'table-sorting.component.html',
	animations: [
		trigger('detailExpand', [
			state('collapsed', style({height: '0px', minHeight: '0'})),
			state('expanded', style({height: '*'})),
			transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
		]),
	],
})

export class TableSortingComponent implements OnInit, OnDestroy {
	user: UsersModel = new UsersModel('', '');
	newUser :FormGroup;
	email = new FormControl('', Validators.required);
	displayname = new FormControl('', Validators.required);

	ngOnInit() {

	}

	constructor(private serverService: ServersService) {
		this.fillForm();
	}

	fillForm() {
		let displayname = '';
		let email = '';
		var validatorArr = [];
		validatorArr.push(Validators.required);
		validatorArr.push(Validators.minLength(1));
		// validatorArr.push(ConfirmPasswordValidator.MatchPassword(this.password));

		this.newUser = new FormGroup({
			displayname: new FormControl(displayname, Validators.required),
			email: new FormControl(email, Validators.required),
		});

	}

	Create() {
		this.user.DisplayName = this.newUser.controls['displayname'].value;
		this.user.Email = this.newUser.controls['email'].value;
		console.log(this.user);
		this.serverService.create(this.user).subscribe();

	}

	ngOnDestroy() {
	}
}

