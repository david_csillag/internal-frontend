import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatChipInputEvent, MatDialogRef} from '@angular/material';
import {PublisherModel} from '../../../../../model/publisher.model';
import {PublisherService} from '../../../../../services/publisher.service';
import {PublisherStorageService} from '../../../../../shared/publisher-storage.service';
import {ProjectService} from '../../../../../services/project.service';
import {ProjectStorageService} from '../../../../../shared/project-storage.service';
import {ProjectModel} from '../../../../../model/project.model';
import {TagModel} from '../../../../../model/tag.model';
import {SourceModel} from '../../../../../model/source.model';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {SourceStorageService} from '../../../../../shared/source-storage.service';
import {SourceService} from '../../../../../services/source.service';

@Component({
	selector: 'source-dialog',
	templateUrl: './source-dialog.component.html',
	styleUrls: ['./source-dialog.component.scss']
})
export class SourceDialogComponent implements OnInit {
	inputOptions = ['name', 'page_from', 'page_to', 'base_problem', 'motivation', 'method', 'result', 'summary'];
	publishers: PublisherModel[] = [];
	projects: ProjectModel[];
	sourceTags: TagModel[] = [];
	filteredPublisherOption: Observable<PublisherModel[]>;
	states = ['COMPLETED', 'INCOMPLETED', 'IN_PROGRESS'];
	sourceFormGroup: FormGroup;

	//chips
	visible = true;
	selectable = true;
	removable = true;
	addOnBlur = true;
	readonly separatorKeysCodes: number[] = [ENTER, COMMA];

	//end chips

	constructor(
		public dialogRef: MatDialogRef<SourceDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: SourceModel,
		private publisherService: PublisherService,
		private publisherStorageService: PublisherStorageService,
		private projectService: ProjectService,
		private projectStorageService: ProjectStorageService,
		private sourceService: SourceService,
		private sourceStorageService : SourceStorageService
	) {
		this.publisherStorageService.getPublishers()
			.subscribe(
				data => {
					this.publisherService.setPublishers(data);
					this.publishers = this.publisherService.getPublishers();
					console.log('GET Request is successful', data);
				},
				error => {
					console.log('GET Request is failed!', error);
				}
			);
		this.projectStorageService.getProjects()
			.subscribe(
				data => {
					this.projectService.setProjects(data);
					this.projects = this.projectService.getProjects();
					console.log('GET Request is successful', data);
				},
				error => {
					console.log('GET Request is failed!', error);
				}
			);
	}

	ngOnInit() {
		this.initForm();
		this.filteredPublisherOption = this.sourceFormGroup.controls['publisher'].valueChanges
			.pipe(
				startWith(''),
				map(value => typeof value === 'string' ? value : value.name),
				map(name => name ? this.publisherFilter(name) : [])
			);
		// this.publisherFormControl.setValue({publisher : this.data.publisher.name});
		// this.publisherFormControl.controls['publisher'].setValue(this.data.publisher.name);
	}

	private initForm() {
		let sourceName = '';
		let sourceState = '';
		let sourceBase_problem = '';
		let sourceMotivation = '';
		let sourceMethod = '';
		let sourceResult = '';
		let sourceSummary = '';
		let sourcePage_to: number;
		let sourcePage_from: number;
		// let sourcePublisher :PublisherModel;
		let sourcePublisher = new PublisherModel('');
		let sourceProject: ProjectModel;
		let sourcePublisherGroup: FormGroup;
		if (this.data) {
			sourceName = this.data.name;
			sourceState = this.data.state;
			sourceBase_problem = this.data.base_problem;
			sourceMotivation = this.data.motivation;
			sourceMethod = this.data.method;
			sourceResult = this.data.result;
			sourceSummary = this.data.summary;
			sourcePage_to = this.data.page_to;
			sourcePage_from = this.data.page_from;
			this.sourceTags = this.data.tags;
			sourcePublisher = this.data.publisher;
			sourceProject = this.data.project;
		}
		sourcePublisherGroup = new FormGroup({
			name: new FormControl(sourcePublisher.name)
		});

		this.sourceFormGroup = new FormGroup({
			name: new FormControl(sourceName, Validators.required),
			state: new FormControl(sourceState, Validators.required),
			base_problem: new FormControl(sourceBase_problem, Validators.required),
			motivation: new FormControl(sourceMotivation, Validators.required),
			method: new FormControl(sourceMethod, Validators.required),
			result: new FormControl(sourceResult, Validators.required),
			summary: new FormControl(sourceSummary, Validators.required),
			page_from: new FormControl(sourcePage_from, Validators.pattern(/^[1-9]+[0-9]*$/)),
			page_to: new FormControl(sourcePage_to, Validators.pattern(/^[1-9]+[0-9]*$/)),
			tags: new FormControl(this.sourceTags, Validators.required),
			publisher: sourcePublisherGroup,
			project: new FormControl(sourceProject, Validators.required)
		});
	}


	publisherAutocomplete(publisher?: PublisherModel): string | undefined {
		return publisher ? publisher.name : undefined;
	}

	onNoClick(): void {
		console.log('onNoClick');
		this.dialogRef.close(null);
	}

	onSubmit() {
		console.log('onSubmit');
		this.sourceService.updateSource(this.sourceService.getSources().indexOf(this.data), this.sourceFormGroup.value);
		this.dialogRef.close(this.sourceFormGroup.value);
	}

	private publisherFilter(name: string) {
		if (name.length > 2) {
			const filterValue = name.toLowerCase();
			return this.publishers.filter(publisher => publisher.name.toLowerCase().indexOf(filterValue) === 0);
		}
		return null;
	}

	projectCompare(project1?: ProjectModel, project2?: ProjectModel) {
		if (project1 && project2) {
			return project1.name === project2.name;
		} else {
			false;
		}
	}

	add(event: MatChipInputEvent): void {
		const input = event.input;
		const value = event.value;

		// Add our tags
		if ((value || '').trim()) {
			this.sourceTags.push({name: value.trim()});
		}

		// Reset the input value
		if (input) {
			input.value = '';
		}
	}

	remove(tag: TagModel): void {
		const index = this.sourceTags.indexOf(tag);

		if (index >= 0) {
			this.sourceTags.splice(index, 1);
		}
	}

}
