import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {
	MatAutocomplete,
	MatAutocompleteSelectedEvent,
	MatChipInputEvent,
	MatDialog,
	MatPaginator,
	MatSort,
	MatTableDataSource
} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {FormControl} from '@angular/forms';
import {Observable, Subscription} from 'rxjs';
import {SourceModel} from '../../../../model/source.model';
import {SourceService} from '../../../../services/source.service';
import {SourceStorageService} from '../../../../shared/source-storage.service';
import {map, startWith} from 'rxjs/operators';
import {SourceDialogComponent} from './source-dialog/source-dialog.component';

/**
 * @title Table with expandable rows
 */
@Component({
	selector: 'source-table',
	styleUrls: ['source-table.component.scss'],
	templateUrl: 'source-table.component.html',
	animations: [
		trigger('detailExpand', [
			state('collapsed', style({height: '0px', minHeight: '0'})),
			state('expanded', style({height: '*'})),
			transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
		]),
	],
})

export class SourceTableComponent implements OnInit {
	dataSource = new MatTableDataSource<any>();
	columnsToDisplay = ['select', 'name', 'state', 'result', 'actions'];
	filterOptions = ['name', 'state', 'base_problem', 'motivation', 'method', 'result', 'summary', 'publisher'];
	publisherFilterOptions = ['name'];
	selection = new SelectionModel<any>(true, []);
	expandedElement: SourceModel | null;
	filterValue: string;
	filteredTags: Observable<string[]>;
	enteredRow: SourceModel;
	subscription: Subscription;

	//tag
	visible = true;
	selectable = true;
	removable = true;
	addOnBlur = true;
	separatorKeysCodes: number[] = [ENTER, COMMA];
	tagCtrl = new FormControl();
	tags: string[] = [];
	allTags: string[] = [];

	@ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
	@ViewChild(MatSort, {static: true}) sort: MatSort;

	@ViewChild('tagInput', {static: false}) tagInput: ElementRef<HTMLInputElement>;
	@ViewChild('auto', {static: false}) matAutocomplete: MatAutocomplete;

	constructor(private sourceService: SourceService,
				private sourceStorageService: SourceStorageService,
				public dialog: MatDialog) {
		this.filteredTags = this.tagCtrl.valueChanges.pipe(
			startWith(null),
			map((tag: string | null) => tag ? this._filter(tag) : []/*this.allTags.slice()*/));
	}

	ngOnInit() {
		this.sourceStorageService.getSources()
			.subscribe(
				data => {
					this.sourceService.setSources(data);
					this.dataSource.data = this.sourceService.getSources();
					this.dataSource.data.forEach(f => {
						f.tags.forEach(tag => {
							if (!this.allTags.includes(tag.name)) {
								this.allTags.push(tag.name);
							}
						});
					});
					console.log('GET Request is successful', data);
					console.log('Tags: ', this.allTags);
				},
				error => {
					console.log('GET Request is failed!', error);
				}
			);
		this.filterValue = '';
		this.dataSource.paginator = this.paginator;
		this.dataSource.sort = this.sort;
		this.dataSource.filterPredicate = (data: SourceModel, filter: string) => {
			let value = '';
			this.filterOptions.forEach(f => {
				if (f.includes('publisher')) {
					value += data.publisher.name;
				} else {
					value += data[f].trim();
				}
			});
			console.log(value);
			return (value.toLowerCase().indexOf(this.filterValue) !== -1 || value.toLowerCase().indexOf(this.filterValue) !== -1) &&
				(this.tags.findIndex(f => !value.toLowerCase().includes(f.toLowerCase()) &&
					!value.toLowerCase().includes(f.toLowerCase()) &&
					!data.tags.includes(data.tags.find(p => p.name === f))
				) === -1)
				;
		};
	}


	/** Whether the number of selected elements matches the total number of rows. */
	isAllSelected() {
		const numSelected = this.selection.selected.length;
		const numRows = this.dataSource.data.length;
		return numSelected === numRows;
	}

	isAllSelectedOnCurrentPage() {
		const numSelected = this.selection.selected.length;
		const numRows = this.paginator.hasNextPage() ?
			this.paginator.pageSize :
			(this.dataSource.filteredData.length - (this.paginator.pageIndex * this.paginator.pageSize));
		return (numSelected === numRows) || (numSelected === this.dataSource.filteredData.length);
	}

	/** Selects all rows if they are not all selected; otherwise clear selection. */
	masterToggle() {
		this.isAllSelectedOnCurrentPage() ?
			this.selection.clear() :
			this.dataSource._orderData(this.dataSource.filteredData).slice(this.dataSource.paginator.pageSize * this.dataSource.paginator.pageIndex,
				this.dataSource.paginator.pageSize * this.dataSource.paginator.pageIndex + this.paginator.pageSize)
				.forEach(row => {
					this.selection.select(row);
				});
	}

	/** The label for the checkbox on the passed row */
	checkboxLabel(row?: SourceModel): string {
		if (!row) {
			return `${this.isAllSelectedOnCurrentPage() ? 'select' : 'deselect'} all`;
		}
		// @ts-ignore
		return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
	}

	paginatorOnClick() {
		this.selection.clear();
	}

	public applyFilter(filterValue?: string) {
		if (this.selection.hasValue()) {
			this.selection.clear();
		}
		this.filterValue = filterValue.toLowerCase();
		this.dataSource.filter = ' ';
		// if (this.tags.length > 0) {
		// 	this.dataSource.connect().next(this.dataSource.filteredData.filter(f => this.tags.indexOf(f.state) != -1));
		// }
	}

	add(event: MatChipInputEvent): void {
		// Add tag only when MatAutocomplete is not open
		// To make sure this does not conflict with OptionSelected Event
		if (!this.matAutocomplete.isOpen) {
			const value = event.value;
			const input = event.input;

			// Add our tag
			if ((value || '').trim()) {
				let tag = value.trim();
				if (!this.tags.includes(tag)) {
					this.tags.push(tag);
					this.applyFilter(this.filterValue);
				}
				if (!this.allTags.includes(tag)) {
					this.allTags.push(tag);
				}
			}

			// Reset the input value
			if (input) {
				input.value = '';
			}
			this.tagCtrl.setValue(null);
			//this.applyFilter(this.filterValue);
		} else {
			console.log(event.input);
			this.matAutocomplete._keyManager.setActiveItem(0);
		}
	}

	remove(tag: string): void {
		const index = this.tags.indexOf(tag);
		if (index >= 0) {
			this.tags.splice(index, 1);
			this.applyFilter(this.filterValue);
		}
		console.log('remove');
	}

	selected(event: MatAutocompleteSelectedEvent): void {
		this.tags.push(event.option.viewValue);
		this.tagInput.nativeElement.value = '';
		this.tagCtrl.setValue(null);
		// if (this.tags.length > 0) {
		this.applyFilter(this.filterValue);
		// }
		console.log('selected');
	}


	deleteRow(row?: SourceModel) {
		if (!row) {
			this.dataSource.data = this.dataSource.data.filter(f => {
				return this.selection.selected.indexOf(f) === -1;
			});
			this.selection.clear();
		} else {
			const index = this.dataSource.data.indexOf(row);
			this.selection.deselect(row);
			this.dataSource.data.splice(index, 1);
		}
		this.dataSource._updateChangeSubscription();
	}

	hoveredRow(row: any) {
		this.enteredRow = row;
	}

	IsdeleteIconHidden(row: any) {
		return !this.selection.isSelected(row) && row != this.enteredRow;
	}

	//legördülő lista előállítása
	private _filter(value: string): string[] {
		const filterValue = value.toLowerCase();
		//chips hossza > n then legördülő lista
		this.applyFilter(this.filterValue);
		if (filterValue.length > 1) {
			return this.allTags.filter(tag => (!this.tags.includes(tag)) && (tag.toLowerCase().indexOf(filterValue) === 0));
		}
		return [];
	}

	openDialog(row?: SourceModel): void {
		const dialogRef = this.dialog.open(SourceDialogComponent, {
			disableClose: true,
			autoFocus: true,
			width: '70%',
			height: '80%',
			data: row ? row : null
		});

		dialogRef.afterClosed().subscribe(result => {
				if (result) {
					const index = this.dataSource.data.indexOf(row);
					if (index > -1) {
						this.dataSource.data[index] = result;
					} else {
						this.dataSource.data.push(result);
					}
				}
				this.dataSource._updateChangeSubscription();
				console.log(result);
			}
		);
	}
}
