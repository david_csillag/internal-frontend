// Angular
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
// Core Module
import {SourceTableComponent} from './source-table.component';
import {
	MatAutocompleteModule,
	MatButtonModule,
	MatButtonToggleModule,
	MatCheckboxModule,
	MatChipsModule, MatDialogModule,
	MatFormFieldModule,
	MatIconModule,
	MatInputModule,
	MatPaginatorModule, MatSelectModule,
	MatSortModule,
	MatTableModule
} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CdkTableModule} from '@angular/cdk/table';
import { SourceDetailElementComponent } from './source-detail-element/source-detail-element.component';
import { SourceDialogComponent } from './source-dialog/source-dialog.component';

@NgModule({
	imports: [
		CommonModule,
		MatTableModule,
		MatSortModule,
		MatPaginatorModule,
		MatCheckboxModule,
		MatInputModule,
		MatChipsModule,
		MatIconModule,
		ReactiveFormsModule,
		MatAutocompleteModule,
		MatButtonModule,
		FormsModule,
		CdkTableModule,
		MatButtonToggleModule,
		MatDialogModule,
		MatFormFieldModule,
		MatSelectModule,
	],
	providers: [],
	exports: [
		SourceTableComponent,
	],
	declarations: [
		SourceTableComponent,
		SourceDetailElementComponent,
		SourceDialogComponent,
	]
	,
	entryComponents: [SourceDialogComponent],

})
export class SourceTableModule {
}
