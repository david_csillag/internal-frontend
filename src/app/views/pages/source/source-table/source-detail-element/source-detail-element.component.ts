import {Component, Input, OnInit} from '@angular/core';
import {SourceModel} from '../../../../../model/source.model';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
	selector: 'source-detail-element',
	templateUrl: './source-detail-element.component.html',
	styleUrls: ['./source-detail-element.component.scss']
})
export class SourceDetailElementComponent implements OnInit {
	@Input()
	source: SourceModel;
	options: FormGroup;
	editable = false;

	constructor(fb: FormBuilder) {
		this.options = fb.group({
			floatLabel: 'auto',
		});
	}

	ngOnInit() {
	}
}
