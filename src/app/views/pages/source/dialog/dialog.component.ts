import {Component, ElementRef, Inject, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatAutocomplete, MatAutocompleteSelectedEvent, MatChipInputEvent, MatPaginator, MatSnackBar, MatSort} from '@angular/material';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {COMMA, ENTER, SEMICOLON} from '@angular/cdk/keycodes';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Observable, Subscription} from 'rxjs';
import {ServersService} from '../../services/servers.service';
// import {ProjectModel} from '../../models/project.model';
// import {SourceModel} from '../../models/source.model';
// import {PublisherModel} from '../../models/publisher.model';
import {map} from 'rxjs/operators';
// import {TagModel} from '../../models/tag.model';
import {KeycloakService} from 'keycloak-angular';
// import {AuthorModel} from '../../models/author.model';
import {UsersModel} from '../../models/users.model';

@Component({
	selector: 'kt-dialog',
	templateUrl: './dialog.component.html',
	styleUrls: ['./dialog.component.scss']
})
export class DialogComponent implements OnInit, OnDestroy {


	visible = true;
	selectable = true;
	removable = true;
	addOnBlur = true;
	editMode = false;
	message: string;
	separatorKeysCodes: number[] = [ENTER, COMMA, SEMICOLON];
	tagCtrl = new FormControl();
	tags: string[] = [];
	// allTags: TagModel[] = [];
	publisher: FormGroup;
	authorId: FormGroup;
	// existingTags: TagModel[] = [];
	states: string[] = ['COMPLETED', 'INCOMPLETE', 'IN_PROCESS'];
	types: string[] = ['ABSTRACT', 'DISSERTATION', 'CONFERENCE_ARTICLE', 'JOURNAL_ARTICLE', 'BOOK'];
	dataColumn: string[] = ['name', 'state', 'project', 'type', 'publisher', 'author', 'dateOfIssue', 'base_problem', 'motivation',
		'method', 'result', 'summary', 'page_from', 'page_to', 'tags'];
	newDataColumn: string[] = ['Név', 'Állapot', 'Project', 'Típus', 'Szerző', 'Kiadó', 'Kiadás éve', 'Alapprobléma', 'Motiváció',
		'Módszertan', 'Eredmény', 'Összegzés', 'Kezdő oldal', 'Befejező oldal', 'Kulcsszavak'];
	options: string[] = [];
	authors: string[] = [];
	projects: string[] = [];
	// filteredOptions: Observable<PublisherModel[]>;
	// filteredProjects: ProjectModel[] = [];
	// filteredAuthors: Observable<AuthorModel[]>;
	filteredTagOptions: Observable<string[]>;
	sub: Subscription;
	subs: Subscription;
	subsc: Subscription;
	// allProjects: ProjectModel[] = [];
	// allPublishers: PublisherModel[] = [];
	// allAuthors: AuthorModel[] = [];
	// publishers: Observable<PublisherModel[]>;
	project: FormGroup;
	controlForm: FormGroup;
	filterValue;

	@ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
	@ViewChild(MatSort, {static: true}) sort: MatSort;
	@ViewChild('tagInput', {static: false}) tagInput: ElementRef<HTMLInputElement>;
	@ViewChild('auto', {static: false}) matAutocomplete: MatAutocomplete;
	@ViewChild('autos', {static: false}) matTagAutocomplete: MatAutocomplete;
	@ViewChild('hidden', {static: false}) hidden: boolean;
	@ViewChild('pubInput', {static: false}) pubInput: string;
	@ViewChild('authorInput', {static: false}) authorInput: string;
	@ViewChild('chipList', {static: false}) chipList;
	@ViewChild('select', {static: false}) select;

	constructor(
		public dialogRef: MatDialogRef<DialogComponent>,
		// @Inject(MAT_DIALOG_DATA) public data: SourceModel,
		private serverService: ServersService,
		private fb: FormBuilder, public snackBar: MatSnackBar,
		private keycloak: KeycloakService) {
		this.filterValue = '';

		// if (this.data.tags.length > 0) {
		// 	this.data.tags.forEach(f => this.tags.push(f.name));
		// }
		//
		// if (data) {
		// 	this.allTags = this.data.tags;
		// } else {
		// 	this.allTags = [];
		// }
		// this.existingTags = [];
		// this.allAuthors = [];
		// this.allPublishers = [];

	}

	ngOnInit(): void {
		this.filterValue = '';

		// if (this.data.project) {
		// 	this.allTags = [].concat(this.data.tags);
		// 	this.editMode = true;
		// 	this.message = 'Source edited successfully!';
		// } else {
		// 	this.allTags = [];
		// 	this.editMode = false;
		// 	this.message = 'Source created successfully!';
		// }
		this.initForm();
		if (this.controlForm.value === null) {
			this.editMode = false;
		}
		// this.serverService.getTags().subscribe(data => {
		// 	this.existingTags = data;
		// 	if(!data){
		// 		this.existingTags=[];
		// 	}
		// 	console.log(this.existingTags);
		// 	return data;
		// });
		//
		// this.sub = this.serverService.getAllProjects().subscribe(
		// 	data => {
		// 		this.allProjects = data;
		// 		this.filteredProjects = data;
		// 		if (data) {
		// 			this.allProjects.forEach(f => this.projects.push(f.name));
		// 		}
		// 		console.log('GET Project Request successful!', data);
		// 	}
		// );

		// this.subs = this.serverService.getAllPublishers().subscribe(
		// 	data => {
		// 		this.allPublishers = data;
		// 		if (data) {
		// 			this.allPublishers.forEach(f => this.options.push(f.name));
		// 		}
		// 		console.log(this.options);
		// 		console.log('GET Publisher Request successful!', data);
		// 	}
		// );
		//
		// this.subsc = this.serverService.getAllAuthors().subscribe(
		// 	data => {
		// 		this.allAuthors = data;
		// 		if (data) {
		// 			this.allAuthors.forEach(f => this.authors.push(f.name));
		// 		}
		// 		console.log(this.options);
		// 		console.log('GET Author Request successful!', data);
		// 	}
		// );

		// this.filteredOptions = this.controlForm.controls['publisher'].valueChanges
		// 	.pipe(
		// 		map(value => typeof value === 'string' ? value : value.name),
		// 		map((publisher: string | null) => publisher ? this._filter(publisher) : []));
		//
		// this.filteredAuthors = this.controlForm.controls['authorId'].valueChanges
		// 	.pipe(
		// 		map(value => typeof value === 'string' ? value : value.name),
		// 		map((authorId: string | null) => authorId ? this._filterAuthors(authorId) : []));


		// this.filteredTagOptions = this.tagCtrl.valueChanges
		// 	.pipe(
		// 		map(tag => tag ? this._filterTags(tag) : [])
		// 	);
	}

	private _filter(event: string) {
		this.filterValue = event.toLowerCase();
		// if (this.allPublishers) {
		// 	return this.allPublishers.filter(option => option.name.toLowerCase().includes(this.filterValue));
		// } else {
		// 	return [];
		// }
	}

	private _filterAuthors(event: string) {
		this.filterValue = event.toLowerCase();
		// if (this.allAuthors) {
		// 	return this.allAuthors.filter(option => option.name.toLowerCase().includes(this.filterValue));
		// }
	}

	private _filterProjects(event: string) {
		this.filterValue = event.toLowerCase();

		// return this.allProjects.filter(option => option.name.toLowerCase().includes(this.filterValue));
	}

	add(event: MatChipInputEvent): void {
		const input = event.input;
		const value = event.value.trim();
		if (!this.matTagAutocomplete.isOpen) {
			// Add our tags
			if ((value || '')) {
				// if (this.allTags.length > 0) {
				// 	if (!this.allTags.find(f => f.name === value)) {
				// 		if (this.existingTags.length>0) {
				// 			let tag = this.existingTags.find(f => f.name === value);
				// 			if (tag) {
				// 				this.allTags.push(tag);
				// 			}
				// 		} else {
				// 			this.allTags.push({id: -1, name: value});
				// 		}
				// 	}
				// } else {
				// 	this.allTags.push({id: -1, name: value});
				// }
			}
			this.chipListChecker();
		}
		// Reset the input value
		if (input) {
			input.value = '';
		}
	}

	tagSelected(event: MatAutocompleteSelectedEvent): void {
		console.log('selected: ' + event.option.value);
		const tagName = event.option.value;
		// if (this.allTags) {
		// 	if (!this.allTags.find(f => f.name === tagName)) {
		// 		this.allTags.push(this.existingTags.find(f => f.name === tagName));
		// 	}
		// }
		this.chipListChecker();
		this.tagInput.nativeElement.value = '';
		this.tagCtrl.setValue(null);
		// if (this.tags.length > 0) {
		// this.applyFilter(this.filterValue);
		// }
		console.log('selected');
	}

	private _filterTags(name: string) {
		const filterValue = name.toLowerCase();
		// if (this.allTags.length > 0 && this.existingTags) {
		// 	return this.existingTags.filter(tag => (tag.name.toLowerCase().indexOf(filterValue) === 0)
		// 		&& (this.allTags.find(f => f.name !== tag.name))).map(m => m.name);
		// } else {
		// 	// return this.existingTags.filter(tag => tag.name.toLowerCase().indexOf(filterValue) === 0).map(m => m.name);
		// 	return [];
		// }
	}

	remove(/*tag: TagModel*/): void {
		// const index = this.allTags.indexOf(tag);
		//
		// if (index >= 0) {
		// 	this.allTags.splice(index, 1);
		// }

	}

	selected(event: MatAutocompleteSelectedEvent): void {
		if (!this.tags.includes(event.option.viewValue)) {
			this.tags.push(event.option.viewValue);
		}
		this.filterValue = '';
		this.tagInput.nativeElement.value = '';
		this.tagCtrl.setValue(null);
	}

	publisherSelected(event: MatAutocompleteSelectedEvent): void {
		// let publ: PublisherModel = this.allPublishers.find(f => f.name === event.option.viewValue);
		// this.controlForm.controls['publisher'].setValue({id: publ.id, name: publ.name});
	}

	publisherCreate(value): void {
		// if (this.allPublishers) {
		// 	if (!this.allPublishers.find(f => f.name === value)) {
		// 		this.controlForm.controls['publisher'].setValue({id: -1, name: value});
		// 	}
		// }
	}

	authorSelected(event: MatAutocompleteSelectedEvent): void {
		// let authorId: AuthorModel = this.allAuthors.find(f => f.name === event.option.viewValue);
		// this.controlForm.controls['authorId'].setValue({id: authorId.id, name: authorId.name});
	}

	authorCreate(value): void {
		// if (this.allAuthors) {
		// 	if (!this.allAuthors.find(f => f.name === value) && value.length>=1) {
		// 		this.controlForm.controls['authorId'].setValue({id: -1, name: value});
		// 	}
		// }
	}

	onNoClick(): void {
		this.dialogRef.close();
	}

	CompareProject(/*cmpW: ProjectModel, cmpT: ProjectModel*/) {
		// if (cmpW && cmpT) {
		// 	return (cmpW.id === cmpT.id);
		// } else {
		// 	return false;
		// }
	}

	private initForm() {
		let id: number;
		let name: string;
		let state: string;
		let base_problem: string;
		let motivation: string;
		let method: string;
		let result: string;
		let summary: string;
		let page_from: number;
		let page_to: number;
		// let project: ProjectModel;
		let publisherName: string;
		let authorName: string;
		// let tags: TagModel[];
		let dateOfIssue: number;
		let type: string;
		 let created_by: UsersModel;
		// let modified_by: UsersModel;


		// const source = this.data;
		// id = source.id;
		// name = source.name;
		// state = source.state;
		// base_problem = source.base_problem;
		// motivation = source.motivation;
		// method = source.method;
		// result = source.result;
		// summary = source.summary;
		// page_from = source.page_from;
		// page_to = source.page_to;
		// project = source.project;
		// publisherName = source.publisher.name;
		// authorName = source.authorId.name;
		// type = source.type;
		// dateOfIssue = source.dateOfIssue;
		this.publisher = new FormGroup({
			// id: new FormControl(source.publisher.id),
			name: new FormControl(publisherName, Validators.required)
		});
		this.authorId = new FormGroup({
			// id: new FormControl(source.authorId.id),
			name: new FormControl(authorName, Validators.minLength(1))
		});

		// this.project = new FormGroup({
		// 	id: new FormControl(source.project.id),
		// 	name: new FormControl(source.project.id, Validators.required),
		// 	cityId: new FormControl(source.project.cityId),
		// 	description: new FormControl(source.project.description),
		// 	created_by: new FormControl(source.project.created_by),
		// 	modified_by: new FormControl(source.project.modified_by)
		// });
		// tags = source.tags;
		// created_by = source.created_by;
		// modified_by = source.modified_by;

		this.controlForm = new FormGroup({
			// id: new FormControl(source.id),
			name: new FormControl(name, Validators.required),
			state: new FormControl(state, Validators.required),
			base_problem: new FormControl(base_problem, Validators.required),
			motivation: new FormControl(motivation, Validators.required),
			method: new FormControl(method, Validators.required),
			result: new FormControl(result, Validators.required),
			summary: new FormControl(summary, Validators.required),
			page_from: new FormControl(page_from, Validators.pattern(/^[1-9]+[0-9]*$/)),
			page_to: new FormControl(page_to, Validators.pattern(/^[1-9]+[0-9]*$/)),
			// project: new FormControl(project, Validators.required),
			// project: this.project,
			publisher: this.publisher,
			authorId: this.authorId,
			dateOfIssue: new FormControl(dateOfIssue, Validators.compose([Validators.pattern(/^[1-9]+[0-9]*$/), Validators.maxLength(4)])),
			// tags: new FormControl(this.allTags, Validators.minLength(1)),
			type: new FormControl(type),
			// created_by: new FormControl(this.data.created_by),
			// modified_by: new FormControl(this.data.modified_by)
		});
	}

	onSubmit() {
		// if (this.editMode && !this.compare()) {
		// 	// console.log(this.controlForm.controls['created_by'].value);
		// 	// this.serverService.putSource(this.controlForm.value).subscribe();
		// } else if (!this.editMode) {
		// 	// console.log(this.controlForm.controls['created_by'].value);
		// 	// this.serverService.createSource(this.controlForm.value).subscribe();
		// }
		this.openSnackBar(this.message, 'Ok');
		this.dialogRef.close('result');
	}

	compare()/*: boolean */{
		// return (this.data.name === this.controlForm.controls['name'].value) &&
		// 	(this.data.base_problem === this.controlForm.controls['base_problem'].value) &&
		// 	(this.data.motivation === this.controlForm.controls['motivation'].value) &&
		// 	(this.data.method === this.controlForm.controls['method'].value) &&
		// 	(this.data.result === this.controlForm.controls['result'].value) &&
		// 	(this.data.summary === this.controlForm.controls['summary'].value) &&
		// 	(this.data.page_to === this.controlForm.controls['page_to'].value) &&
		// 	(this.data.page_from === this.controlForm.controls['page_from'].value) &&
		// 	(this.data.state === this.controlForm.controls['state'].value) &&
		// 	(this.data.project.id === this.controlForm.controls['project'].value.id) &&
		// 	(this.data.publisher.name === this.publisher.controls['name'].value) &&
		// 	(this.allTags.filter(f => this.data.tags.map(m => m.name).includes(f.name)).length === this.allTags.length &&
		// 		this.data.tags.length === this.allTags.length);
	}

	chipListChecker() {
		// this.chipList.errorState = this.allTags.length < 1;
	}


	openSnackBar(message: string, action: string) {
		this.snackBar.open(message, action, {
			duration: 2000,
		});
	}

	ngOnDestroy(): void {
	}
}
