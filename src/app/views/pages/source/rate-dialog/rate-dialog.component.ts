import {Component, ElementRef, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {NgbRating} from '@ng-bootstrap/ng-bootstrap';
import {ServersService} from '../../services/servers.service';
// import {RatingModel} from '../../models/rating.model';
import {KeycloakService} from 'keycloak-angular';
// import {SourceModel} from '../../models/source.model';

@Component({
	selector: 'kt-rate-dialog',
	templateUrl: './rate-dialog.component.html',
	styleUrls: ['./rate-dialog.component.scss']
})
export class RateDialogComponent implements OnInit {
	selected: number = 0;
	comment: string;
	// rate: RatingModel = new RatingModel(-1,0,'',null,null,null,null);

	@ViewChild('element', {static: false}) element: ElementRef<HTMLInputElement>;
	@ViewChild('rating', {static: false}) rating: NgbRating;

	ngOnInit() {

	}

	constructor(
		public dialogRef: MatDialogRef<RateDialogComponent>, private serverService: ServersService, private keycloak: KeycloakService,
		/*@Inject(MAT_DIALOG_DATA) public data: SourceModel*/) {
		// this.serverService.getRating(this.data.id, this.keycloak.getUsername()).subscribe(
		// 	data => {
		// 		if (data) {
		// 			this.rate = data;
		// 			this.element.nativeElement.value = data.comment;
		// 			this.selected = data.rating;
		// 		}
		// 	});

	}

	onNoClick(): void {
		this.dialogRef.close();
	}


	onSubmit() {
		console.log('Submit megnyomva');
		this.comment = this.element.nativeElement.value;
		// if (this.rate.id > 0) {
		// 	console.log('Volt rate');
		// 	this.rate.rating = this.selected;
		// 	this.rate.comment = this.comment;
		// 	this.serverService.createRating(this.rate).subscribe();
		// } else {
		// 	console.log('Nem volt rate');
		// 	// let user_id: UsersModel = new UsersModel(0, this.keycloak.getKeycloakInstance().tokenParsed['sub'],
		// 	// 	this.keycloak.getKeycloakInstance().tokenParsed['name'], this.keycloak.getKeycloakInstance().tokenParsed['email']);
		// 	let rt: RatingModel = new RatingModel(-1, this.selected, this.comment, null, this.data, null, null);
		// 	this.serverService.createRating(rt).subscribe();
		//
		// }
		this.dialogRef.close('result');
	}

}
