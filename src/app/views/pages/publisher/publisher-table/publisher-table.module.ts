// Angular
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
// Core Module
import {PublisherTableComponent} from './publisher-table.component';
import {
	MatAutocompleteModule,
	MatButtonModule,
	MatButtonToggleModule,
	MatCheckboxModule,
	MatChipsModule,
	MatIconModule,
	MatInputModule,
	MatPaginatorModule,
	MatSortModule,
	MatTableModule
} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CdkTableModule} from '@angular/cdk/table';

@NgModule({
	imports: [
		CommonModule,
		MatTableModule,
		MatSortModule,
		MatPaginatorModule,
		MatCheckboxModule,
		MatInputModule,
		MatChipsModule,
		MatIconModule,
		ReactiveFormsModule,
		MatAutocompleteModule,
		MatButtonModule,
		FormsModule,
		CdkTableModule,
		MatButtonToggleModule,
	],
	providers: [],
	exports: [
		PublisherTableComponent,
	],
	declarations: [
		PublisherTableComponent,
	]
})
export class PublisherTableModule {
}
