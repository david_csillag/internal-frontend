import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {
	MatAutocomplete,
	MatAutocompleteSelectedEvent,
	MatChipInputEvent,
	MatPaginator,
	MatSort,
	MatTableDataSource
} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {FormControl} from '@angular/forms';
import {Subscription} from 'rxjs';
import {PublisherService} from '../../../../services/publisher.service';
import {PublisherStorageService} from '../../../../shared/publisher-storage.service';
import {PublisherModel} from '../../../../model/publisher.model';
import {ProjectModel} from '../../../../model/project.model';

/**
 * @title Table with expandable rows
 */
@Component({
	selector: 'publisher-table',
	styleUrls: ['publisher-table.component.scss'],
	templateUrl: 'publisher-table.component.html',
	animations: [
		trigger('detailExpand', [
			state('collapsed', style({height: '0px', minHeight: '0'})),
			state('expanded', style({height: '*'})),
			transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
		]),
	],
})
export class PublisherTableComponent implements OnInit {
	dataSource = new MatTableDataSource<any>();
	columnsToDisplay = ['selet', 'name', 'delete'];
	selection = new SelectionModel<any>(true, []);
	expandedElement: PublisherModel | null;
	filterValue: string;
	enteredRow: PublisherModel;
	subscription: Subscription;

	//tag
	visible = true;
	selectable = true;
	removable = true;
	addOnBlur = true;
	separatorKeysCodes: number[] = [ENTER, COMMA];
	tagCtrl = new FormControl();
	tags: string[] = [];

	@ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
	@ViewChild(MatSort, {static: true}) sort: MatSort;

	@ViewChild('tagInput', {static: false}) tagInput: ElementRef<HTMLInputElement>;
	@ViewChild('auto', {static: false}) matAutocomplete: MatAutocomplete;

	constructor(private publisherService: PublisherService,
				private publisherStorageService: PublisherStorageService) {
	}

	ngOnInit() {
		this.publisherStorageService.getPublishers()
			.subscribe(
				data => {
					this.publisherService.setPublishers(data);
					this.dataSource.data = this.publisherService.getPublishers();
					console.log('GET Request is successful', data);
				},
				error => {
					console.log('GET Request is failed!', error);
				}
			);
		this.filterValue = '';
		this.dataSource.paginator = this.paginator;
		this.dataSource.sort = this.sort;
		this.dataSource.filterPredicate = (data: PublisherModel, filter: string) => {
			return (data.name.toLowerCase().indexOf(this.filterValue) !== -1) &&
				(this.tags.findIndex(f => !data.name.toLowerCase().includes(f.toLowerCase())) === -1)
				;
		};
	}


	/** Whether the number of selected elements matches the total number of rows. */
	isAllSelected() {
		const numSelected = this.selection.selected.length;
		const numRows = this.dataSource.data.length;
		return numSelected === numRows;
	}

	isAllSelectedOnCurrentPage() {
		const numSelected = this.selection.selected.length;
		const numRows = this.paginator.hasNextPage() ?
			this.paginator.pageSize :
			(this.dataSource.filteredData.length - (this.paginator.pageIndex * this.paginator.pageSize));
		return (numSelected === numRows) || (numSelected === this.dataSource.filteredData.length);
	}

	/** Selects all rows if they are not all selected; otherwise clear selection. */
	masterToggle() {
		this.isAllSelectedOnCurrentPage() ?
			this.selection.clear() :
			this.dataSource._orderData(this.dataSource.filteredData).slice(this.dataSource.paginator.pageSize * this.dataSource.paginator.pageIndex,
				this.dataSource.paginator.pageSize * this.dataSource.paginator.pageIndex + this.paginator.pageSize)
				.forEach(row => {
					this.selection.select(row);
				});
	}

	/** The label for the checkbox on the passed row */
	checkboxLabel(row?: PeriodicElement): string {
		if (!row) {
			return `${this.isAllSelectedOnCurrentPage() ? 'select' : 'deselect'} all`;
		}
		// @ts-ignore
		return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
	}

	paginatorOnClick() {
		this.selection.clear();
	}

	public applyFilter(filterValue?: string) {
		if (this.selection.hasValue()) {
			this.selection.clear();
		}
		this.filterValue = filterValue.toLowerCase();
		this.dataSource.filter = ' ';
		// if (this.tags.length > 0) {
		// 	this.dataSource.connect().next(this.dataSource.filteredData.filter(f => this.tags.indexOf(f.state) != -1));
		// }
	}

	add(event: MatChipInputEvent): void {
		// Add tag only when MatAutocomplete is not open
		// To make sure this does not conflict with OptionSelected Event
		const input = event.input;
		const value = event.value;

		// Add our fruit
		if ((value || '').trim()) {
			if (!this.tags.includes(value.trim())) {
				this.tags.push(value.trim());
				this.applyFilter(this.filterValue);
			}
		}

		// Reset the input value
		if (input) {
			input.value = '';
		}
	}

	remove(tag: string): void {
		const index = this.tags.indexOf(tag);
		if (index >= 0) {
			this.tags.splice(index, 1);
			this.applyFilter(this.filterValue);
		}
		console.log('remove');
	}

	selected(event: MatAutocompleteSelectedEvent): void {
		this.tags.push(event.option.viewValue);
		this.tagInput.nativeElement.value = '';
		this.tagCtrl.setValue(null);
		// if (this.tags.length > 0) {
		this.applyFilter(this.filterValue);
		// }
		console.log('selected');
	}


	deleteRow(row?: PeriodicElement) {
		if (!row) {
			this.dataSource.data = this.dataSource.data.filter(f => {
				return this.selection.selected.indexOf(f) === -1;
			});
			this.selection.clear();
		} else {
			const index = this.dataSource.data.indexOf(row);
			this.selection.deselect(row);
			this.dataSource.data.splice(index, 1);
		}
		this.dataSource._updateChangeSubscription();
	}

	addRow() {
		let newElement: PeriodicElement =
			{
				position: 0,
				name: '',
				weight: 0,
				symbol: '',
				description: ``,
				state: ''
			};
		this.dataSource.data.push(newElement);
		this.dataSource._updateChangeSubscription();
	}

	hoveredRow(row: any) {
		this.enteredRow = row;
	}

	IsdeleteIconHidden(row: any) {
		return !this.selection.isSelected(row) && row != this.enteredRow;
	}
}

export interface PeriodicElement {
	name: string;
	position: number;
	weight: number;
	symbol: string;
	description: string;
	state: string
}

const ELEMENT_DATA: PeriodicElement[] = [
	{
		position: 1,
		name: 'Hydrogen',
		weight: 1.0079,
		symbol: 'H',
		description: `Hydrogen is a chemical element with symbol H and atomic number 1. With a standard
        atomic weight of 1.008, hydrogen is the lightest element on the periodic table.`,
		state: 'Gas'
	}, {
		position: 2,
		name: 'Helium',
		weight: 4.0026,
		symbol: 'He',
		description: `Helium is a chemical element with symbol He and atomic number 2. It is a
        colorless, odorless, tasteless, non-toxic, inert, monatomic gas, the first in the noble gas
        group in the periodic table. Its boiling point is the lowest among all the elements.`,
		state: 'Gas'
	}, {
		position: 3,
		name: 'Lithium',
		weight: 6.941,
		symbol: 'Li',
		description: `Lithium is a chemical element with symbol Li and atomic number 3. It is a soft,
        silvery-white alkali metal. Under standard conditions, it is the lightest metal and the
        lightest solid element.`,
		state: 'Solid'
	}, {
		position: 4,
		name: 'Beryllium',
		weight: 9.0122,
		symbol: 'Be',
		description: `Beryllium is a chemical element with symbol Be and atomic number 4. It is a
        relatively rare element in the universe, usually occurring as a product of the spallation of
        larger atomic nuclei that have collided with cosmic rays.`,
		state: 'Solid'
	}, {
		position: 5,
		name: 'Boron',
		weight: 10.811,
		symbol: 'B',
		description: `Boron is a chemical element with symbol B and atomic number 5. Produced entirely
        by cosmic ray spallation and supernovae and not by stellar nucleosynthesis, it is a
        low-abundance element in the Solar system and in the Earth's crust.`,
		state: 'Solid'
	}, {
		position: 6,
		name: 'Carbon',
		weight: 12.0107,
		symbol: 'C',
		description: `Carbon is a chemical element with symbol C and atomic number 6. It is nonmetallic
        and tetravalent—making four electrons available to form covalent chemical bonds. It belongs
        to group 14 of the periodic table.`,
		state: 'Solid'
	}, {
		position: 7,
		name: 'Nitrogen',
		weight: 14.0067,
		symbol: 'N',
		description: `Nitrogen is a chemical element with symbol N and atomic number 7. It was first
        discovered and isolated by Scottish physician Daniel Rutherford in 1772.`,
		state: 'Gas'
	}, {
		position: 8,
		name: 'Oxygen',
		weight: 15.9994,
		symbol: 'O',
		description: `Oxygen is a chemical element with symbol O and atomic number 8. It is a member of
         the chalcogen group on the periodic table, a highly reactive nonmetal, and an oxidizing
         agent that readily forms oxides with most elements as well as with other compounds.`,
		state: 'Gas'
	}, {
		position: 9,
		name: 'Fluorine',
		weight: 18.9984,
		symbol: 'F',
		description: `Fluorine is a chemical element with symbol F and atomic number 9. It is the
        lightest halogen and exists as a highly toxic pale yellow diatomic gas at standard
        conditions.`,
		state: 'Gas'
	}, {
		position: 10,
		name: 'Neon',
		weight: 20.1797,
		symbol: 'Ne',
		description: `Neon is a chemical element with symbol Ne and atomic number 10. It is a noble gas.
        Neon is a colorless, odorless, inert monatomic gas under standard conditions, with about
        two-thirds the density of air.`,
		state: 'Gas'
	},
];


/**  Copyright 2019 Google Inc. All Rights Reserved.
 Use of this source code is governed by an MIT-style license that
 can be found in the LICENSE file at http://angular.io/license */
