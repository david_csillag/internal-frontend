import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {
	MatAutocompleteModule, MatButtonModule,
	MatCheckboxModule,
	MatChipsModule,
	MatFormFieldModule,
	MatIconModule,
	MatInputModule,
	MatPaginatorModule
} from '@angular/material';
import {TableSortingComponent} from './table-sorting.component';
import {ReactiveFormsModule} from '@angular/forms';



@NgModule({
	declarations: [TableSortingComponent],
	exports: [
		TableSortingComponent
	],
	imports: [
		CommonModule,
		MatTableModule,
		MatSortModule,
		MatPaginatorModule,
		MatCheckboxModule,
		MatFormFieldModule,
		MatInputModule,
		MatChipsModule,
		MatIconModule,
		ReactiveFormsModule,
		MatAutocompleteModule,
		MatButtonModule,
	]
})
export class TableSortingModule { }
