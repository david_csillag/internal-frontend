import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatAutocomplete, MatAutocompleteSelectedEvent, MatChipInputEvent, MatSort} from '@angular/material';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {SelectionModel} from '@angular/cdk/collections';
import {map, startWith} from 'rxjs/operators';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {COMMA, ENTER} from '@angular/cdk/keycodes';

/**
 * @title Table with pagination
 */
@Component({
	selector: 'table-sorting',
	styleUrls: ['table-sorting.component.scss'],
	templateUrl: 'table-sorting.component.html',
	animations: [
		trigger('detailExpand', [
			state('collapsed', style({height: '0px', minHeight: '0'})),
			state('expanded', style({height: '*'})),
			transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
		]),
	],
})
export class TableSortingComponent implements OnInit {
	displayedColumns: string[] = ['position', 'name', 'weight', 'symbol', 'select', 'delete'];

	dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);

	expandedElement: PeriodicElement | null;

	selection = new SelectionModel<PeriodicElement>(true, []);

	pageIndex: number;
	filteredLength: number = 0;
	filterValue: string;
	hidden: boolean;


	visible = true;
	selectable = true;
	removable = true;
	addOnBlur = true;
	separatorKeysCodes: number[] = [ENTER, COMMA];
	tagCtrl = new FormControl();
	filteredTags: Observable<string[]>;
	tags: string[] = [];
	allTags: string[] = ['gas', 'solid'];

	@ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
	@ViewChild(MatSort, {static: true}) sort: MatSort;

	@ViewChild('tagInput', {static: false}) tagInput: ElementRef<HTMLInputElement>;
	@ViewChild('auto', {static: false}) matAutocomplete: MatAutocomplete;

	@ViewChild('actualRow', {static: true}) actualRow: boolean;

	isAllSelected() {
		const numSelected = this.selection.selected.length;
		let numRows: number = this.paginator.pageSize;
		if (!this.paginator.hasNextPage()) {
			numRows = this.dataSource.filteredData.length -(this.paginator.pageSize * this.paginator.pageIndex);
		}
		return numSelected === numRows;
	}


	masterToggle() {
		this.isAllSelected() ?
			this.selection.clear() :
			this.dataSource._orderData(this.dataSource.filteredData)
				.slice(this.pageIndex * this.paginator.pageSize, (1 + this.paginator.pageIndex) * this.paginator.pageSize)
				.forEach(row => this.selection.select(row));
	}

	checkboxLabel(row?: PeriodicElement): string {
		if (!row) {
			return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
		}
		return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
	}

	pageChange() {
		this.selection.clear();
		if (this.pageIndex === this.paginator.pageIndex - 1) {
			this.pageIndex += 1;
		} else if (this.pageIndex === this.paginator.pageIndex + 1) {
			this.pageIndex -= 1;
		} else if (this.paginator._nextButtonsDisabled()) {
			this.pageIndex = this.paginator.pageIndex;
		} else {
			this.pageIndex = 0;
		}
	}

	constructor() {
		this.filteredTags = this.tagCtrl.valueChanges.pipe(
			map((tag: string | null) => tag ? this._filter(tag) : []));
	}

	add(event: MatChipInputEvent): void {
		// Add fruit only when MatAutocomplete is not open
		// To make sure this does not conflict with OptionSelected Event
		if (!this.matAutocomplete.isOpen) {
			const input = event.input;
			const value = event.value;

			// Add our tag
			if ((value || '').trim()) {
				if (!this.tags.includes(value.trim())) {
					this.tags.push(value.trim());
					this.applyFilter(this.filterValue);
				}
			}

			// Reset the input value
			if (input) {
				input.value = '';
			}

			this.tagCtrl.setValue(null);
		}else {
			this.matAutocomplete._keyManager.setActiveItem(0);
		}
	}

	remove(tag: string): void {
		const index = this.tags.indexOf(tag);

		if (index >= 0) {
			this.tags.splice(index, 1);
			this.applyFilter(this.filterValue);
		}

	}

	selected(event: MatAutocompleteSelectedEvent): void {
		if (!this.tags.includes(event.option.viewValue)) {
			this.tags.push(event.option.viewValue);
		}
		this.applyFilter(this.filterValue);
		this.tagInput.nativeElement.value = '';
		this.tagCtrl.setValue(null);
	}

	applyFilter(value) {
		if(this.selection.hasValue()){
			this.selection.clear();
		}
		if(this.filterValue){
			this.filterValue = value.toLowerCase().trim();
		}
		this.dataSource.filter = ' ';
	}

	private _filter(value: string): string[] {
		this.filterValue = value.toLowerCase().trim();
		if (this.filterValue.length >= this.filteredLength) {
			this.filteredLength = this.filterValue.length;
			this.applyFilter(this.filterValue);
		}
		return this.allTags.filter(tag => !this.tags.includes(tag) && tag.toLowerCase().indexOf(this.filterValue) > -1);
	}

	actRow(row?: PeriodicElement){
		this.dataSource.data.splice(this.dataSource.data.indexOf(row),1);
		this.selection.deselect(row);
		this.dataSource._updateChangeSubscription();
	}

	clearAll(){
		for(let data = 0; data < this.dataSource.filteredData.length; data++){
			if(this.selection.isSelected(this.dataSource.data[this.dataSource.data.indexOf(this.dataSource.filteredData[data])])){
				this.dataSource.data.splice(this.dataSource.data.indexOf(this.dataSource.filteredData[data]), 1);
				this.dataSource._updateChangeSubscription();
				data--;
			}
		}

		this.selection.clear();

	}

	ngOnInit() {
		this.hidden = true;
		this.filterValue='';
		this.dataSource.paginator = this.paginator;
		this.dataSource.sort = this.sort;
		this.pageIndex = this.paginator.pageIndex;
		this.dataSource.filterPredicate =
			(data: PeriodicElement, filter: string) => {
				if (data.name.toLowerCase().indexOf(this.filterValue) !== -1) {
					if (this.tags.length === 0) {
						return true;
					} else if(this.tags.indexOf(data.state) > -1){
						return true;
					}
				} else {
					return false;
				}
			};
	}
}


export interface PeriodicElement {
	name: string;
	position: number;
	weight: number;
	symbol: string;
	state: string;
	description: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
	{
		position: 1,
		name: 'Hydrogen',
		weight: 1.0079,
		symbol: 'H',
		state: 'gas',
		description: `Hydrogen is a chemical element with symbol H and atomic number 1. With a standard
        atomic weight of 1.008, hydrogen is the lightest element on the periodic table.`
	}, {
		position: 2,
		name: 'Helium',
		weight: 4.0026,
		symbol: 'He',
		state: 'gas',
		description: `Helium is a chemical element with symbol He and atomic number 2. It is a
        colorless, odorless, tasteless, non-toxic, inert, monatomic gas, the first in the noble gas
        group in the periodic table. Its boiling point is the lowest among all the elements.`
	}, {
		position: 3,
		name: 'Lithium',
		weight: 6.941,
		symbol: 'Li',
		state: 'solid',
		description: `Lithium is a chemical element with symbol Li and atomic number 3. It is a soft,
        silvery-white alkali metal. Under standard conditions, it is the lightest metal and the
        lightest solid element.`
	}, {
		position: 4,
		name: 'Beryllium',
		weight: 9.0122,
		symbol: 'Be',
		state: 'solid',
		description: `Beryllium is a chemical element with symbol Be and atomic number 4. It is a
        relatively rare element in the universe, usually occurring as a product of the spallation of
        larger atomic nuclei that have collided with cosmic rays.`
	}, {
		position: 5,
		name: 'Boron',
		weight: 10.811,
		symbol: 'B',
		state: 'solid',
		description: `Boron is a chemical element with symbol B and atomic number 5. Produced entirely
        by cosmic ray spallation and supernovae and not by stellar nucleosynthesis, it is a
        low-abundance element in the Solar system and in the Earth's crust.`
	}, {
		position: 6,
		name: 'Carbon',
		weight: 12.0107,
		symbol: 'C',
		state: 'solid',
		description: `Carbon is a chemical element with symbol C and atomic number 6. It is nonmetallic
        and tetravalent—making four electrons available to form covalent chemical bonds. It belongs
        to group 14 of the periodic table.`
	}, {
		position: 7,
		name: 'Nitrogen',
		weight: 14.0067,
		symbol: 'N',
		state: 'gas',
		description: `Nitrogen is a chemical element with symbol N and atomic number 7. It was first
        discovered and isolated by Scottish physician Daniel Rutherford in 1772.`
	}, {
		position: 8,
		name: 'Oxygen',
		weight: 15.9994,
		symbol: 'O',
		state: 'gas',
		description: `Oxygen is a chemical element with symbol O and atomic number 8. It is a member of
         the chalcogen group on the periodic table, a highly reactive nonmetal, and an oxidizing
         agent that readily forms oxides with most elements as well as with other compounds.`
	}, {
		position: 9,
		name: 'Fluorine',
		weight: 18.9984,
		symbol: 'F',
		state: 'gas',
		description: `Fluorine is a chemical element with symbol F and atomic number 9. It is the
        lightest halogen and exists as a highly toxic pale yellow diatomic gas at standard
        conditions.`
	}, {
		position: 10,
		name: 'Neon',
		weight: 20.1797,
		symbol: 'Ne',
		state: 'gas',
		description: `Neon is a chemical element with symbol Ne and atomic number 10. It is a noble gas.
        Neon is a colorless, odorless, inert monatomic gas under standard conditions, with about
        two-thirds the density of air.`
	},
];
