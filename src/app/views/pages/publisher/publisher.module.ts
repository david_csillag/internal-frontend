// Angular
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
// Core Module
import { CoreModule } from '../../../core/core.module';
import { PartialsModule } from '../../partials/partials.module';
import { PublisherComponent } from './publisher.component';
import {SortingTableModule} from '../../themes/demo1/content/sortingTable/sortingTable.module';
import {PublisherTableModule} from './publisher-table/publisher-table.module';

@NgModule({
	imports: [
		CommonModule,
		PartialsModule,
		CoreModule,
		RouterModule.forChild([
			{
				path: '',
				component: PublisherComponent
			},
		]),
		SortingTableModule,
		PublisherTableModule,
	],
	providers: [],
	declarations: [
		PublisherComponent,
	]
})
export class PublisherModule {
}
