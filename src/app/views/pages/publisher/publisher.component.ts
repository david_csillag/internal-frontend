// Angular
import {Component, OnDestroy, OnInit} from '@angular/core';
import {PublisherModel} from '../../../model/publisher.model';
import {Subscription} from 'rxjs';
import {PublisherService} from '../../../services/publisher.service';
import {ActivatedRoute, Router} from '@angular/router';
import {PublisherStorageService} from '../../../shared/publisher-storage.service';

@Component({
	selector: 'publisher-dashboard',
	templateUrl: './publisher.component.html',
	styleUrls: ['publisher.component.scss'],
})
export class PublisherComponent implements OnInit, OnDestroy {
	publishers: PublisherModel[] = [];
	subscription: Subscription;

	constructor(private publisherService: PublisherService,
				private router: Router,
				private route: ActivatedRoute,
				private publisherStorageService: PublisherStorageService
	) {
	}

	ngOnInit() {
		this.subscription = this.publisherService.publisherChanged.subscribe(
			(publishers: PublisherModel[]) => {
				this.publishers = publishers;
			}
		);
		this.publishers = this.publisherService.getPublishers();
	}

	onNewPublisher() {
		this.router.navigate(['new'], {relativeTo: this.route});
	}

	ngOnDestroy() {
		this.subscription.unsubscribe();
	}

	onLoadPublishers() {
		this.publisherStorageService.getPublishers()
			.subscribe(
				data => {
					this.publisherService.setPublishers(data);
					console.log('GET Request is succesful ', data);
				},
				error => {
					console.log('Error', error);
				}
			);
	}

}
