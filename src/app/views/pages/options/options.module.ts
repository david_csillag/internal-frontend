import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {OptionsComponent} from './options.component';
import {MatButtonModule, MatInputModule} from '@angular/material';
import {PartialsModule} from '../../partials/partials.module';
import {RouterModule} from '@angular/router';
import {FormsModule} from '@angular/forms';


@NgModule({
	declarations: [OptionsComponent],
	exports: [
		OptionsComponent,
	],
	imports: [
		CommonModule,
		PartialsModule,
		MatButtonModule,
		RouterModule.forChild([
			{
				path: '',
				component: OptionsComponent
			},
		]),
		MatInputModule,
		FormsModule,
	],
	entryComponents: [OptionsComponent]
})
export class OptionsModule { }
