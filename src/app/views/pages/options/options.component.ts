import {Component, OnInit} from '@angular/core';
import {ExternalAuthOptionModel} from '../../../model/external_auth_option.model';
import {UserService} from '../../../shared/user.service';
import {ExternalBindingModel} from '../../../model/externalBinding.model';
import {switchMap} from 'rxjs/operators';

@Component({
	selector: 'kt-options',
	templateUrl: './options.component.html',
	styleUrls: ['./options.component.scss']
})
export class OptionsComponent implements OnInit {
	DisplayName: string;
	Email: string;
	socials: ExternalAuthOptionModel[] = [
		{
			authId: 0,
			connected: false,
			email: '',
			icon: 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Google_%22G%22_Logo.svg/512px-Google_%22G%22_Logo.svg.png',
			type: 4
		},
		{
			authId: 0,
			connected: false,
			email: '',
			icon: 'http://icons.iconarchive.com/icons/danleech/simple/256/facebook-icon.png',
			type: 2
		}
	];

	constructor(private userService: UserService) {
		console.log('request...');
		this.userService.getUserBindings(this.getToken()).subscribe(
			data => {
				let externals: ExternalBindingModel = data;
				this.socials[0].email = externals[0].email;
				this.socials[0].authId = externals[0].authId;
				this.socials[0].connected = externals[0].email.length > 0;
				this.socials[1].email = externals[1].email;
				this.socials[1].connected = externals[1].email.length > 0;
				this.socials[1].authId = externals[1].authId;
				console.log(externals);
			}
		);
		// route.queryParams.subscribe(data => {
		//   const _token = data['token'];
		//   if (_token) {
		// 	  console.log('Token: ' + _token);
		// 	  token = _token;
		// 	  window.sessionStorage.setItem('t', token);
		// 	  this.router.navigateByUrl('~');
		//   }
		// });
	}

	ngOnInit() {
	}

	connect(type: number) {
		console.log('connect: ' + type);
		this.userService.connectExternalProfile(type).subscribe(
			data => {
				window.location.href = data;
			}
		);
	}

	disconnect(external: ExternalAuthOptionModel) {
		console.log(this.socials);
		var db = 0;
		for (var social of this.socials) {
			if (social.connected) {
				db++;
			}
		}
		if (db > 1) {
			console.log('disconnect');
			this.userService.disconnectExternalProfile(external).subscribe(
				data => {
					var ext = this.socials.find(x => x.authId == external.authId);
					ext.connected = false;
					ext.email = '';
					ext.authId = 0;
				}
			);
		}
	}


	private getToken() {
		return sessionStorage.getItem('token');
	}
}
