import {Component, OnInit} from '@angular/core';
import {RegisterUserModel} from '../models/registerUser.model';
import {FacebookService} from '../../../shared/facebook.service';
import {OwnAuthService} from '../../../shared/ownAuth.service';
import {GoogleService} from '../../../shared/google.service';
import {ActivatedRoute, CanActivate, Route, Router, RouterLink, RouterModule} from '@angular/router';
import {HttpClient, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {AjaxResponse} from 'rxjs/ajax';
import {RouterAction} from '@ngrx/router-store';
import {isNotNullOrUndefined} from 'codelyzer/util/isNotNullOrUndefined';
import {UsersModel} from '../models/users.model';
import {ServersService} from '../services/servers.service';


@Component({
	selector: 'kt-authentication',
	templateUrl: './authentication.component.html',
	styleUrls: ['./authentication.component.scss'],
})
export class AuthenticationComponent implements OnInit {
	user: UsersModel = new UsersModel('', '');
	// loggedIn = new Observable<string>((observer: Observer<string>)=>{
	// 	setInterval(()=>observer.next(window.location.href), 10);
	// });
	sessionStorage = sessionStorage.length;
	success: boolean;

	hiddenSignIn = sessionStorage.length>0;
	hiddenSignOut = !this.hiddenSignIn;

	constructor(private server: ServersService) {
		this.hiddenSignIn = sessionStorage.length>0;
		this.hiddenSignOut = !this.hiddenSignIn;
	}

	ngOnInit(): void {
		this.sessionStorage = sessionStorage.length;
		this.success = false;

	}

	Social(): void {
		this.server.loginUser().subscribe(data => window.location.href = data);
	}



	Signout(): void {
		this.server.logoutUser().subscribe(data => {
			console.log(data);
			this.success = data;
			if (this.success === true) {
				sessionStorage.clear();
				location.reload();
			}
		});
	}


	googleRegistration() {
		this.server.register().subscribe(
			data => {
				console.log(data);
				window.location.href = data;
				// window.open(data,"popup", 'width=600,height=800').moveTo(window.screen.width / 2 - 300,window.screen.height / 2 - 400) ;
			}
		);
	}
}
