import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatButtonModule, MatInputModule} from '@angular/material';
import {PartialsModule} from '../../partials/partials.module';
import {RouterModule} from '@angular/router';
import {FormsModule} from '@angular/forms';
import {AuthenticationComponent} from './authentication.component';


@NgModule({
	declarations: [AuthenticationComponent],
	exports: [
		AuthenticationComponent,
	],
	imports: [
		CommonModule,
		PartialsModule,
		MatButtonModule,
		RouterModule.forChild([
			{
				path: '',
				component: AuthenticationComponent
			},
		]),
		MatInputModule,
		FormsModule,
	],
	entryComponents: [AuthenticationComponent]
})
export class AuthenticationModule { }
