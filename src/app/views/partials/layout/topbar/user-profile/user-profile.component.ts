// Angular
import {Component, Input, OnInit} from '@angular/core';
// RxJS
// NGRX
// State
import {KeycloakService} from 'keycloak-angular';
import {AuthService, SocialUser} from 'angularx-social-login';
import {Router} from '@angular/router';

declare var FB: any;
@Component({
	selector: 'kt-user-profile',
	templateUrl: './user-profile.component.html',
})
export class UserProfileComponent implements OnInit {
	// Public properties
	user: SocialUser = new SocialUser();
	loggedIn = false;
	userInitials: string;


	@Input() avatar: boolean = true;
	@Input() greeting: boolean = true;
	@Input() badge: boolean;
	@Input() icon: boolean;

	/**
	 * Component constructor
	 *
	 * @param keycloak
	 * @param authService
	 * @param router
	 */
	constructor(public keycloak: KeycloakService, private authService: AuthService, private router: Router) {
		this.loggedIn=false;
	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit(): void {
		// this.loggedIn = false;
		//
		// (window as any).fbAsyncInit = function () {
		// 	FB.init({
		// 		appId: '460574287872798',
		// 		cookie: false,
		// 		xfbml: false,
		// 		version: 'v4.0',
		// 	});
		//
		// 	FB.AppEvents.logPageView();
		// 	FB.getLoginStatus(function (response) {
		// 		if (response.status === 'connected') {
		// 			console.log(response.authResponse.accessToken);
		// 		// @*Itt mentem le az access tokent cookie-ként*@
		// 		// 		document.cookie = 'at ='+ response.authResponse.accessToken;
		// 		}
		// 	});
		// };
		//
		//
		// (function (d, s, id) {
		// 	var js, fjs = d.getElementsByTagName(s)[0];
		// 	if (d.getElementById(id)) { return; }
		// 	js = d.createElement(s); js.id = id;
		// 	js.src = "https://connect.facebook.net/en_US/sdk.js";
		// 	fjs.parentNode.insertBefore(js, fjs);
		// }(document, 'script', 'facebook-jssdk'));
		// // this.user = this.keycloak.getKeycloakInstance().tokenParsed['name'];
		// // this.userInitials = this.keycloak.getKeycloakInstance().tokenParsed['given_name'].charAt(0)+this.keycloak.getKeycloakInstance().tokenParsed['family_name'].charAt(0);
	}

	/**
	 * Log out
	 */
	// logout() {
	// 	this.keycloak.logout();
	// }
	login(): void{
		// FB.login((response)=>
		// {
		// 	console.log('submitLogin',response);
		// 	if (response.authResponse)
		// 	{
		// 		//login success
		// 		//login success code here
		// 		//redirect to home page
		// 		this.loggedIn = true;
		// 		this.user = response.authResponse.user;
		// 		// window.location.assign('https://localhost:5001/login');
		//
		// 	}
		// 	else
		// 	{
		// 		console.log('User login failed');
		// 	}
		// });
	}
	logout(): void {
		// this.authService.signOut();
		this.loggedIn = false;
		this.user = new SocialUser();
		// window.location.assign('https://facebook.com/logout.php?next=https://localhost:5001/&access_token='+this.user.authToken);

	}
}
