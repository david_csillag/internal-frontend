export class MenuConfig {
	public defaults: any = {
		header: {
			self: {},
			items: [
				{
					title: 'Dashboards',
					root: true,
					alignment: 'left',
					page: '/projects',
					translate: 'MENU.DASHBOARD',
				},

			]
		},
		aside: {
			self: {},
			items: [
				{
					title: 'User list',
					root: true,
					icon: 'flaticon2-architecture-and-city',
					page: '/userList',
					translate: 'MENU.DASHBOARD',
					bullet: 'dot',
				},{
					title: 'User details',
					root: true,
					icon: 'flaticon-user',
					page: '/userDetails',
				},
				// {
				// 	title: 'Authentication',
				// 	root: true,
				// 	icon: 'flaticon2-shield',
				// 	page: '/authentication',
				// },
				{
					title: 'Login',
					root: true,
					icon: 'flaticon2-user-outline-symbol',
					page: '/login',
				},
				{
					title: 'Register',
					root: true,
					icon: 'flaticon2-user-outline-symbol',
					page: '/register',
				},
			]
		},
	};

	public get configs(): any {
		return this.defaults;
	}
}
