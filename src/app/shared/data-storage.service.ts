import {HttpClient, HttpHeaders} from '@angular/common/http';
import {throwError} from 'rxjs';
import {Injectable} from '@angular/core';

@Injectable()
export class DataStorageService {
	// Base url
	private baseurl = 'https://localhost:5001/api';
	// Http Headers
	private httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json; charset=utf-8;'
		})
	};

	constructor(private httpClient: HttpClient) {
	}


// Error handling
	errorHandl(error) {
		let errorMessage = '';
		if (error.error instanceof ErrorEvent) {
// Get client-side error
			errorMessage = error.error.message;
		} else {
// Get server-side error
			errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
		}
		console.log(errorMessage);
		return throwError(errorMessage);
	}

}
