import {DataStorageService} from './data-storage.service';
import {ProjectModel} from '../model/project.model';
import {Observable} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Injectable} from '@angular/core';

@Injectable()
export class ProjectStorageService extends DataStorageService{

	super() {
	}

	getProjects(): Observable<ProjectModel[]> {
		// @ts-ignore
		return this.httpClient.get<ProjectModel[]>(this.baseurl + '/projects').pipe(
			catchError(this.errorHandl));
	}
}
