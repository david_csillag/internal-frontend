import {DataStorageService} from './data-storage.service';
import {Observable} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {TokenOwnModel} from '../model/tokenOwn.model';
import {UserModel} from '../model/user.model';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {SessionModel} from '../model/session.model';

@Injectable()
export class OwnAuthService extends DataStorageService {

	super() {
	}

	login(email: string, password: string): Observable<TokenOwnModel> {
		let body = {
			'LoginType': 'OWN',
			'Email': email,
			'Password': password
		};
		// @ts-ignore
		return this.httpClient.post<TokenOwnModel>('https://localhost:5001/api/authentication/authenticate', body).pipe(
			catchError(this.errorHandl));
	}

	register(displayName: string, email: string, username: string, password: string, rePassword: string): Observable<UserModel> {
		let body = {
			'RegisterType': '',
			'DisplayName': displayName,
			'Email': email,
			'Username': username,
			'Password': password,
			'RePassword': rePassword,
		};
		// @ts-ignore
		return this.httpClient.post<UserModel>('https://localhost:5001/api/authentication/register', body).pipe(
			catchError(this.errorHandl));
	}

	authGuardTeszt(token: string): Observable<string> {
		const httpOptions = {
			headers: {
				'Authorization': 'Bearer ' + token
			}
		};
		// @ts-ignore
		return this.httpClient.get<string>('https://localhost:5001/api/authentication/teszt', httpOptions).pipe(
			catchError(this.errorHandl));
	}

	getSessions(token: string): Observable<SessionModel[]> {
		const httpOptions = {
			headers: {
				'Authorization': 'Bearer ' + token
			}
		};
		// @ts-ignore
		return this.httpClient.get<SessionModel[]>('https://localhost:5001/api/authentication/teszt', httpOptions).pipe(
			catchError(this.errorHandl));
	}
}
