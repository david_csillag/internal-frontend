import {DataStorageService} from './data-storage.service';
import {Observable} from 'rxjs';
import {PublisherModel} from '../model/publisher.model';
import {catchError} from 'rxjs/operators';
import {SourceModel} from '../model/source.model';

export class SourceStorageService extends DataStorageService{

	super() {
	}

	getSources(): Observable<SourceModel[]> {
		// @ts-ignore
		return this.httpClient.get<PublisherModel[]>(this.baseurl + '/sources').pipe(
			catchError(this.errorHandl));
	}
}
