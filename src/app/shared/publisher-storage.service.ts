import {DataStorageService} from './data-storage.service';
import {Observable} from 'rxjs';
import {PublisherModel} from '../model/publisher.model';
import {catchError} from 'rxjs/operators';

export class PublisherStorageService extends DataStorageService{

	super() {
	}

	getPublishers(): Observable<PublisherModel[]> {
		// @ts-ignore
		return this.httpClient.get<PublisherModel[]>(this.baseurl + '/publishers').pipe(
			catchError(this.errorHandl));
	}
}
