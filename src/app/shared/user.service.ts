import {DataStorageService} from './data-storage.service';
import {ProjectModel} from '../model/project.model';
import {Observable} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {ExternalBindingModel} from '../model/externalBinding.model';
import {ExternalAuthOptionModel} from '../model/external_auth_option.model';

@Injectable()
export class UserService extends DataStorageService{

	super() {
	}

	connectExternalProfile(type : number): Observable<string> {
		// @ts-ignore
		return this.httpClient.get<string>(this.baseurl + '/authentication/social/open?type=' + type).pipe(
			catchError(this.errorHandl));
	}

	disconnectExternalProfile(externalAuthOptionModel : ExternalAuthOptionModel): Observable<string> {
		// @ts-ignore
		return this.httpClient.post<string>(this.baseurl + '/user/social/disconnect?type=' + externalAuthOptionModel.type + '&authId=' + externalAuthOptionModel.authId).pipe(
			catchError(this.errorHandl));
	}

	getUserBindings(token: string): Observable<ExternalBindingModel> {
		const httpOptions = {
			headers: {
				'Authorization': 'Bearer ' + token
			}
		};
		// @ts-ignore
		return this.httpClient.get<ExternalBindingModel>(this.baseurl + '/user/getUserBindings', httpOptions).pipe(
			catchError(this.errorHandl));
	}

}
