import {DataStorageService} from './data-storage.service';
import {ProjectModel} from '../model/project.model';
import {Observable} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Injectable} from '@angular/core';

@Injectable()
export class GoogleService extends DataStorageService{

	super() {
	}

	login(): Observable<string> {
		// @ts-ignore
		return this.httpClient.get<string>(this.baseurl + '/authentication/social/open?type=3').pipe(
			catchError(this.errorHandl));
	}

	register(): Observable<string> {
		// @ts-ignore
		return this.httpClient.get<string>(this.baseurl + '/authentication/social/open?type=4').pipe(
			catchError(this.errorHandl));
	}
}
