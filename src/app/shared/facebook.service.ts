import {DataStorageService} from './data-storage.service';
import {ProjectModel} from '../model/project.model';
import {Observable} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Injectable} from '@angular/core';

@Injectable()
export class FacebookService extends DataStorageService{

	super() {
	}

	login(): Observable<string> {
		// @ts-ignore
		return this.httpClient.get<string>('https://localhost:5001/api/authentication/social/open?type=1').pipe(
			catchError(this.errorHandl));
	}
	register(): Observable<string> {
		// @ts-ignore
		return this.httpClient.get<string>('https://localhost:5001/api/authentication/social/open?type=2').pipe(
			catchError(this.errorHandl));
	}

	logoutUser(token: string):Observable<boolean> {
		// let token = sessionStorage.getItem('t');
		const httpOptions = {
			headers: {
				'Authorization': 'Bearer ' + token
			}
		};
		// @ts-ignore
		return this.httpClient.get<boolean>('https://localhost:5001/api/Authentication/logout', httpOptions).pipe(
			catchError(this.errorHandl));
	}
}
